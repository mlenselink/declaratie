(ns declaratie.table
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   [tick.core :as t]
   ["react-native" :as rn]
   ["react-native-paper" :as rnp]
   ["react-native-vector-icons/MaterialCommunityIcons" :default MaterialCommunityIcons]
   ["react-native-safe-area-context" :refer (SafeAreaView)]
   ["@react-navigation/native" :refer (DrawerActions)]
   ["react-native-table-component" :refer (Table TableWrapper Row Cell)]
   ["react-native-fs" :as RNFS]
   ["react-native-document-picker" :default RNDocumentPicker]
   ["react-native-share" :default Share]
   [declaratie.export :as export]
   [declaratie.holiday :as holiday]
   [declaratie.ref :as ref]
   [declaratie.stopwatch :as stopwatch]
   [declaratie.styles :as styles]
   [declaratie.subs]
   [declaratie.time :as time]))

(def message (r/atom nil))

(def dialog-visible? (r/atom false))

(def internal-directory
  "The absolute path to the app-specific document directory on internal storage. The files are removed on app uninstall."
  RNFS/DocumentDirectoryPath)

(def external-directory
  "The absolute path to the app-specific document directory on external storage. The files are removed on app uninstall."
  RNFS/ExternalDirectoryPath)

(def shared-directory
  "The absolute path to the shared storage. The files are not removed on app uninstall."
  RNFS/ExternalStorageDirectoryPath)

(def downloads-directory
  "The absolute path to the download directory on external storage. The files are not removed on app uninstall."
  RNFS/DownloadDirectoryPath)

(defn unique-filename
  "Return a unique filename.
  If `files` contains `file` a number between parentheses is appended.
  `files` should be a javascript array."
  [files file]
  (if (.includes files file)
    (let [match (re-matches #"(.*)(\.[^.]+)" file)
          [_ base extension] match]
      (loop [number 1]
        (let [new-file (str base "_(" number ")" extension)]
          (if (.includes files new-file)
            (recur (inc number))
            ;; return file with "_(<number)" appended
            new-file))))
    ;; `files` does't contain `file`
    file))

(defn write-file
  [directory file contents encoding]
  ;; First, read the directory to check if the intended filename already exists to prevent overwriting
  (rf/dispatch [:set-loading true])
  (-> (RNFS/readdir directory)
      (.then (fn [files]
               (unique-filename files file))
             ;; An empty directory throws an error, but means that the filename can be safely written
             (fn [error] file))
      (.then (fn [filename] (RNFS/writeFile (str directory "/" filename) contents encoding)))
      (.then #(reset! message "Bestand opgeslagen"))
      (.catch #(rf/dispatch [:set-error-message (.-message %)]))
      (.finally #(rf/dispatch [:set-loading false]))))

(defn share
  [intervals]
  (let [filename (str "declaratie_" (t/today))
        mimetype "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]
    (rf/dispatch [:set-loading true])
    (-> intervals
        (export/intervals->xlsx)
        (.then (fn [contents]
                 (let [url (str "data:" mimetype ";base64," contents)]
                   (rf/dispatch [:set-loading false])
                   (.open Share #js {:filename filename
                                     :url url}))))
        (.catch #(rf/dispatch [:set-error-message (.-message %)])))))

(defn header []
  (let [intervals (rf/subscribe [:get-intervals])
        running? (rf/subscribe [:stopwatch-running?])]
    (fn []
      [:> (.-Header rnp/Appbar)
       [:> (.-Action rnp/Appbar) {:icon "menu"
                                  :on-press #((:dispatch @ref/navigation) (.toggleDrawer DrawerActions))}]
       [:> (.-Content rnp/Appbar)
        {:title "Tabel"}]
       [:> (.-Action rnp/Appbar) {:icon "content-save"
                                  :disabled (empty? @intervals)
                                  :on-press #(reset! dialog-visible? true)}]
       [:> (.-Action rnp/Appbar) {:icon "share-variant"
                                  :disabled (empty? @intervals)
                                  :on-press #(share @intervals)}]
       (if @running?
         [stopwatch/icon])])))

(defn data-table []
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        intervals (rf/subscribe [:get-intervals])]
    (fn []
      (let [header ["Datum" "Dag" "Feestdag" "Bereikbaar" "Gewerkt"]
            rows (map-indexed (fn [index item] (assoc item :index index)) @intervals)
            width [100 100 100 100 100]]
        [:> rn/ScrollView
         {:horizontal true}
         [:> rn/View
          {:style (styles/table-container @dark-theme?)}
          [:> Table
           [:> Row
            {:style (styles/header-row @dark-theme?)
             :text-style (styles/header-text @dark-theme?)
             :data (clj->js header)
             :width-arr (clj->js width)}]]
          [:> rn/ScrollView
           [:> Table
            (doall
             (for [row rows]
               (let [{:keys [start end standby? worked?]} row
                     date (time/format-date start "dd-MM-yyyy")
                     weekday (time/format-date start "EEEE")
                     holiday? (holiday/holiday? (t/date start))
                     range (str (time/format-date start "HH:mm")
                                "-"
                                (if (and (= (t/hour end) 0)
                                         (= (t/minute end) 0))
                                  "24:00"
                                  (time/format-date end "HH:mm")))]
                 ^{:key (:index row)}
                 [:> TableWrapper
                  {:style (styles/table-row @dark-theme?)}
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 0)})
                    :text-style (styles/table-text @dark-theme?)
                    :data date}]
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 1)})
                    :text-style (styles/table-text @dark-theme?)
                    :data weekday}]
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 2)})
                    :data (r/as-element [:> MaterialCommunityIcons
                                         {:name (when holiday? "check")
                                          :color (styles/icon-color @dark-theme?)}])}]
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 3)})
                    :text-style (styles/table-text @dark-theme?)
                    :data (when standby? range)}]
                  [:> Cell
                   {:style (styles/table-cell {:width (nth width 4)})
                    :text-style (styles/table-text @dark-theme?)
                    :data (when worked? range)}]])))]]]]))))

(defn message-bar []
  [:> rnp/Snackbar
   {:visible @message
    :on-dismiss #(reset! message nil)}
   @message])

(defn error-bar []
  (let [error-message (rf/subscribe [:get-error-message])]
    (fn []
      [:> rnp/Snackbar
       {:visible (not (empty? @error-message))
        :on-dismiss #(rf/dispatch [:set-error-message ""])}
       @error-message])))

(defn save-dialog []
  (let [intervals (rf/subscribe [:get-intervals])
        directory (rf/subscribe [:get-storage-directory])
        menu-visible? (r/atom false)
        base (r/atom (str "declaratie_" (t/today)))]
    (fn []
      (when-not @directory (rf/dispatch [:set-storage-directory downloads-directory]))
      [:> rnp/Portal
       [:> rnp/Dialog
        {:visible @dialog-visible?}
        [:> (.-Title rnp/Dialog) "Opslaan als"]
        [:> (.-Content rnp/Dialog)
         [:> rnp/Subheading "Opslaglocatie"]
         [:> rnp/Menu
          {:visible @menu-visible?
           :on-dismiss #(reset! menu-visible? false)
           :anchor (r/as-element [:> rnp/Button
                                  {:content-style {:flex-direction "row-reverse"}
                                   :mode "outlined"
                                   :icon (if @menu-visible? "menu-up" "menu-down")
                                   :uppercase false
                                   :on-press #(reset! menu-visible? true)}
                                  downloads-directory])}
          [:> (.-Item rnp/Menu)
           {:title "Downloads"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (rf/dispatch [:set-storage-directory downloads-directory]))}]
          [:> (.-Item rnp/Menu)
           {:title "Gedeelde opslag"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (rf/dispatch [:set-storage-directory shared-directory]))}]
          [:> (.-Item rnp/Menu)
           {:title "Externe opslag"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (rf/dispatch [:set-storage-directory external-directory]))}]
          [:> (.-Item rnp/Menu)
           {:title "Interne opslag"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (rf/dispatch [:set-storage-directory internal-directory]))}]
          [:> rnp/Divider]
          [:> (.-Item rnp/Menu)
           {:title "Bladeren"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (-> (.pickDirectory RNDocumentPicker)
                            (.then #(rf/dispatch [:set-storage-directory %]))
                            (.catch #(rf/dispatch [:set-error-message (.-message %)]))))}]]
         [:> rnp/Subheading "Bestandsnaam"]
         [:> rnp/TextInput
          {:mode "outlined"
           :value @base
           :on-change-text #(reset! base %)
           :right (r/as-element [:> (.-Affix rnp/TextInput) {:text ".xlsx"}])}]]
        [:> (.-Actions rnp/Dialog)
         [:> rnp/Button
          {:on-press #(reset! dialog-visible? false)}
          "Annuleren"]
         [:> rnp/Button
          {:on-press (fn []
                       (let [filename (str @base ".xlsx")
                             encoding "base64"]
                         (rf/dispatch [:set-loading true])
                         (-> @intervals
                             (export/intervals->xlsx)
                             (.then (fn [contents]
                                      (rf/dispatch [:set-loading false])
                                      (write-file @directory filename contents encoding)))
                             (.catch (fn [error] (rf/dispatch [:set-error-message error])))))
                       (reset! dialog-visible? false))}
          "Opslaan"]]]])))

(defn screen [props]
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        loading? (rf/subscribe [:loading?])
        top (-> (.get rn/Dimensions "window")
                (.-height)
                (- 48) ; size of large indicator
                (/ 2))
        left (-> (.get rn/Dimensions "window")
                 (.-width)
                 (- 48) ; size of large indicator
                 (/ 2))]
    (fn [props]
      [:> SafeAreaView
       {:style {:flex 1}}
       [:> rn/StatusBar
        {:background-color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :primary])
         :status-bar-style (if @dark-theme? "dark-content" "light-content")}]
       [header]
       [data-table]
       [message-bar]
       [error-bar]
       [save-dialog]
       (when @loading?
         [:> rnp/ActivityIndicator
          {:style {:position "absolute"
                   :top top
                   :left left}
           :size "large"}])])))
