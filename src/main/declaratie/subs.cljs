(ns declaratie.subs
  (:require [re-frame.core :as rf]
            [clojure.string :as str]
            [tick.core :as t]
            [declaratie.db :as db]
            [declaratie.interval :as interval]
            [declaratie.time :as time]))

;; Helpers

(defn query-events
  "Return items in EVENTS in which `:title` contains string S.
  EVENTS is a vector of maps.
  Regex matching is case-insensitive"
  [s events]
  (let [regex (str "(?i)" ".*" s ".*")
        pattern (re-pattern regex)
        pred #(re-matches pattern (:title %))]
    (filterv pred events)))

;; Subscriptions
(rf/reg-sub
 :loading?
 (fn [db _]
   (:loading? db)))

(rf/reg-sub
 :get-calendars
 (fn [db _]
   (:calendars db)))

(rf/reg-sub
 :get-calendar-ids
 (fn [db _]
   (->> db
        (:calendar-ids)
        (into []))))

(rf/reg-sub
 :calendar-enabled?
 (fn [db [_ id] ]
   (let [ids (:calendar-ids db)]
     (contains? ids id))))

(rf/reg-sub
 :get-calendar-bounds
 (fn [db _]
   (:calendar-bounds db)))

;; "Return a map of marked dates suitable for using in the calendar.
;; The events returned by `react-native-calendar-events` are contained in a
;; vector of maps. First, a sequence of start dates is created. Second, the
;; sequence is converted to a map with the start date as key, and the marked
;; state as value."
(rf/reg-sub
 :get-marked
 (fn []
   [(rf/subscribe [:get-events])
    (rf/subscribe [:get-current-date])])
 (fn [[events current-date]]
   (let [selected-date (str current-date)
         marked (->> events
                     (map (comp str t/date :start))
                     (reduce #(assoc %1 %2 {:marked true}) {}))]
     (update marked selected-date assoc :selected true))))

(rf/reg-sub
 :get-all-events
 (fn [db _]
   (:events db)))

(rf/reg-sub
 :get-events
 (fn []
   [(rf/subscribe [:get-all-events])
    (rf/subscribe [:get-selected-events])
    (rf/subscribe [:get-event-filter])])
 (fn [[all-events selected-events filter?] _]
   (if filter? selected-events all-events)))

(rf/reg-sub
 :get-query
 (fn [db _]
   (:query db)))

(rf/reg-sub
 :query-events
 (fn []
   [(rf/subscribe [:get-events])
    (rf/subscribe [:get-query])])
 (fn [[events query] _]
   (if query
     (query-events query events)
     events)))

(rf/reg-sub
 :get-selected-events
 (fn [db _]
   (->> db
        (:selected-events)
        (sort-by (juxt :start :end))
        (into []))))

(rf/reg-sub
 :get-event-filter
 (fn [db _]
   (:event-filter db)))

(rf/reg-sub
 :event-selected?
 (fn [event]
   (rf/subscribe [:get-selected-events]))
 (fn [events [_ event]]
   (->> events
        (some #(and (= (:id %) (:id event))
                    (= (:title %) (:title event))
                    (= (:start %) (:start event))
                    (= (:end %) (:end event))))
        (true?))))

(rf/reg-sub
 :get-all-calls
 (fn [db _]
   (:calls db)))

(rf/reg-sub
 :get-selected-call-ids
 (fn [db _]
   (:selected-call-ids db)))

(rf/reg-sub
 :get-selected-calls
 (fn []
   [(rf/subscribe [:get-all-calls])
    (rf/subscribe [:get-selected-call-ids])])
 (fn [[calls ids] _]
   (->> calls
        (filter #(contains? ids (:id %)))
        (into []))))

(rf/reg-sub
 :get-calls
 (fn []
   [(rf/subscribe [:get-all-calls])
    (rf/subscribe [:get-selected-call-ids])
    (rf/subscribe [:get-call-filter])])
 (fn [[calls ids filter?] _]
   (->> calls
        (filter #(if filter?
                   (contains? ids (:id %))
                   (constantly true)))
        (reverse)
        (into []))))

(rf/reg-sub
 :get-call-filter
 (fn [db _]
   (:call-filter db)))

(rf/reg-sub
 :call-selected?
 (fn []
   (rf/subscribe [:get-selected-call-ids]))
 (fn [ids [_ id]]
   (contains? ids id)))

(rf/reg-sub
 :get-all-activities
 (fn [db _]
   (:activities db)))

(rf/reg-sub
 :get-activity
 (fn []
   (rf/subscribe [:get-all-activities]))
 (fn [activities [_ key]]
   (get activities key)))

(rf/reg-sub
 :get-selected-activity-ids
 (fn [db _]
   (:selected-activity-ids db)))

(rf/reg-sub
 :get-selected-activities
 (fn []
   [(rf/subscribe [:get-all-activities])
    (rf/subscribe [:get-selected-activity-ids])])
 (fn [[activities ids] _]
   (->> ids
        (select-keys activities)
        (map (fn [[id activity]] (assoc activity :id id)))
        (sort-by (juxt :start :end))
        (reverse)
        (into []))))

(rf/reg-sub
 :get-activities
 (fn []
   [(rf/subscribe [:get-all-activities])
    (rf/subscribe [:get-selected-activity-ids])
    (rf/subscribe [:get-activity-filter])])
 (fn [[activities ids filter?] _]
   (->> (if filter?
          (select-keys activities ids)
          activities)
        (map (fn [[id activity]] (assoc activity :id id)))
        (sort-by (juxt :start :end))
        (reverse)
        (into []))))

(rf/reg-sub
 :get-activity-filter
 (fn [db _]
   (:activity-filter db)))

(rf/reg-sub
 :activity-selected?
 (fn []
   (rf/subscribe [:get-selected-activity-ids]))
 (fn [ids [_ id]]
   (contains? ids id)))

(rf/reg-sub
 :get-standby-intervals
 (fn []
   (rf/subscribe [:get-selected-events]))
 (fn [events]
   (->> events
        (map interval/round-up-to-half-hour)
        (interval/merge-intervals)
        (map interval/round-off-by-quarter)
        (interval/split-intervals))))

(rf/reg-sub
 :get-worked-intervals
 (fn []
   [(rf/subscribe [:get-selected-calls])
    (rf/subscribe [:get-selected-activities])])
 (fn [[calls activities]]
   (->> (concat calls activities)
        (map interval/round-up-to-half-hour)
        (interval/merge-intervals)
        (map interval/round-off-by-quarter)
        (interval/split-intervals))))

(rf/reg-sub
 :get-intervals
 (fn []
   [(rf/subscribe [:get-standby-intervals])
    (rf/subscribe [:get-worked-intervals])])
 (fn [[standby-intervals worked-intervals]]
   (let [standby (->> standby-intervals
                      (map #(assoc % :standby? true)))
         worked (->> worked-intervals
                     (map #(assoc % :worked? true)))]
     (into [] (concat standby worked)))))

(rf/reg-sub
 :get-current-date
 (fn [db _]
   (:current-date db)))

(rf/reg-sub
 :dark-theme?
 (fn [db _]
   (:dark-theme db)))

(rf/reg-sub
 :get-storage-directory
 (fn [db _]
   (:storage-directory db)))

(rf/reg-sub
 :stopwatch-running?
 (fn [db _]
   (:stopwatch-running? db)))

(rf/reg-sub
 :get-stopwatch-start
 (fn [db _]
   (get-in db [:stopwatch-interval :start])))

(rf/reg-sub
 :get-stopwatch-end
 (fn [db _]
   (get-in db [:stopwatch-interval :end])))

(rf/reg-sub
 :calendar-visible?
 (fn [db _]
   (:calendar-visible? db)))

(rf/reg-sub
 :stopwatch-visible?
 (fn [db _]
   (:stopwatch-visible? db)))

(rf/reg-sub
 :get-personal-name
 (fn [db _]
   (:personal-name db)))

(rf/reg-sub
 :get-personal-number
 (fn [db _]
   (:personal-number db)))

(rf/reg-sub
 :get-personal-premium
 (fn [db _]
   (:personal-premium db)))

(rf/reg-sub
 :get-calls-limit-count
 (fn [db _]
   (:calls-limit-count db)))

(rf/reg-sub
 :get-calls-limit-period
 (fn [db _]
   (:calls-limit-period db)))

(rf/reg-sub
 :get-error-message
 (fn [db _]
   (:error-message db)))
