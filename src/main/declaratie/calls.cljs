(ns declaratie.calls
  (:require
   [goog.object :as gobj]
   [reagent.core :as r]
   [clojure.string :as str]
   [tick.core :as t]
   [re-frame.core :as rf]
   ["react" :as react]
   ["react-native" :as rn]
   ["react-native-paper" :as rnp]
   ["react-native-safe-area-context" :refer (SafeAreaView)]
   ["@react-navigation/native" :refer (DrawerActions)]
   [declaratie.events]
   [declaratie.ref :as ref]
   [declaratie.stopwatch :as stopwatch]
   [declaratie.styles :as styles]
   [declaratie.subs]
   [declaratie.time :as time]))

(def dialog-visible? (r/atom false))

(defn dialog []
  [:> rnp/Portal
   [:> rnp/Dialog
    {:visible @dialog-visible?}
    [:> (.-Title rnp/Dialog) "Alle oproepen opnieuw laden?"]
    [:> (.-Content rnp/Dialog)
     [:> rnp/Paragraph
      "Gegevens die niet in de oproepgeschiedenis zijn opgeslagen zullen verloren gaan."]]
    [:> (.-Actions rnp/Dialog)
     [:> rnp/Button
      {:on-press #(reset! dialog-visible? false)}
      "Annuleren"]
     [:> rnp/Button
      {:on-press (fn []
                   (rf/dispatch [:load-calls :init])
                   (reset! dialog-visible? false))}
      "OK"]]]])

(defn header []
  (let [filter-menu-visible? (r/atom false)
        call-filter (rf/subscribe [:get-call-filter])
        running? (rf/subscribe [:stopwatch-running?])]
    (fn []
      [:> (.-Header rnp/Appbar)
       [:> (.-Action rnp/Appbar)
        {:icon "menu"
         :on-press #((:dispatch @ref/navigation) (.toggleDrawer DrawerActions))}]
       [:> (.-Content rnp/Appbar)
        {:title "Oproepen"}]
       [:> rnp/Menu
        {:anchor (r/as-element [:> rnp/IconButton
                                {:icon "filter"
                                 :color "white"
                                 :on-press #(swap! filter-menu-visible? not)}])
         :visible  @filter-menu-visible?
         :on-dismiss #(reset! filter-menu-visible? false)}
        [:> (.-Group rnp/RadioButton)
         {:on-value-change (fn [value]
                             (rf/dispatch [:set-call-filter value])
                             (reset! filter-menu-visible? false))
          :value @call-filter}
         [:> (.-Item rnp/RadioButton)
          {:label "Alle"
           :value false}]
         [:> (.-Item rnp/RadioButton)
          {:label "Geselecteerd"
           :value true}]]]
       [:> (.-Action rnp/Appbar)
        {:icon "refresh"
         :on-press #(reset! dialog-visible? true)}]
       (if @running?
         [stopwatch/icon])])))

(defn error-bar []
  (let [error-message (rf/subscribe [:get-error-message])]
    (fn []
      [:> rnp/Snackbar
       {:visible (not (empty? @error-message))
        :on-dismiss #(rf/dispatch [:set-error-message ""])}
       @error-message])))

(defn checkbox [id]
  (let [selected? (rf/subscribe [:call-selected? id])]
    (fn [id]
      [:> rnp/Checkbox {:status (if @selected? "checked" "unchecked")
                        :on-press #(rf/dispatch [:toggle-call id])}])))

(defn render-item [props]
  (let [{{raw-type :rawType
          phone-number :phoneNumber
          duration :duration
          name :name
          id :id
          start :start
          end :end
          :as call} :item
         index :index
         separators :separators} props
        icon (case raw-type
               ;; Call log type for incoming calls.
               1 "phone-incoming"
               ;; Call log type for outgoing calls.
               2 "phone-outgoing"
               ;; Call log type for missed calls.
               3 "phone-missed"
               ;; Call log type for voicemails.
               4 "phone-message"
               ;; Call log type for calls rejected by direct user action.
               5 "phone-cancel"
               ;; Call log type for calls blocked automatically.
               6 "phone-remove"
               ;; Call log type for a call which was answered on another device.
               ;; Used in situations where a call rings on multiple devices
               ;; simultaneously and it ended up being answered on a device
               ;; other than the current one.
               7 "location-exit")
        number (if (str/blank? phone-number) "Privénummer" phone-number)
        sender (if (str/blank? name) number name)]
    [:> (.-Item rnp/List)
     {:title sender
      :description (str (time/format-date start "dd-MM-yyyy HH:mm") "\n"
                        duration "s")
      :description-number-of-lines 2
      :left #(r/as-element [checkbox id])
      :right #(r/as-element [:> rnp/IconButton {:icon icon}])}]))

(defn call-list []
  (let [calls (rf/subscribe [:get-calls])]
    (fn []
      [:> rn/FlatList
       {:data (clj->js @calls)
        :key-extractor (fn [item index] (gobj/get item "timestamp"))
        :render-item #(r/as-element [render-item (js->clj % :keywordize-keys true)])
        :ItemSeparatorComponent #(r/as-element [:> rnp/Divider])}])))

(defn screen [props]
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        loading? (rf/subscribe [:loading?])
        calls (rf/subscribe [:get-calls])
        top (-> (.get rn/Dimensions "window")
                (.-height)
                (- 48) ; size of large indicator
                (/ 2))
        left (-> (.get rn/Dimensions "window")
                 (.-width)
                 (- 48) ; size of large indicator
                 (/ 2))]
    (fn [props]
      [:> SafeAreaView
       [:> rn/StatusBar
        {:background-color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :primary])
         :status-bar-style (if @dark-theme? "dark-content" "light-content")}]
       [header]
       (cond
         (empty? @calls) [:<>
                          [:> rnp/Paragraph "Geen oproepen gevonden"]
                          [:> rn/View {:style {:flex-direction "row"}}
                           [:> rnp/Paragraph "Geef de app toegang tot de gesprekslijsten in de "]
                           [:> rnp/Button
                            {:mode "outlined"
                             :label-style {:font-size 14
                                           :line-height 20
                                           :margin-vertical 2}
                             :uppercase false
                             :on-press #(.openSettings rn/Linking)}
                            "instellingen"]]
                          [:> rn/View {:style {:flex-direction "row"}}
                           [:> rnp/Paragraph "Klik daarna op "]
                           [:> rnp/Button
                            {:mode "outlined"
                             :label-style {:font-size 14
                                           :line-height 20
                                           :margin-vertical 2}
                             :uppercase false
                             :on-press #(rf/dispatch [:load-events])}
                            "verversen"]]]
         :else [call-list])
       [dialog]
       [error-bar]
       (when @loading?
         [:> rnp/ActivityIndicator
          {:style {:position "absolute"
                   :top top
                   :left left}
           :size "large"}])])))
