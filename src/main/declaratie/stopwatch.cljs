(ns declaratie.stopwatch
  (:require
   [reagent.core :as r]
   [re-frame.core :as rf]
   ["react-native" :as rn]
   ["react-native-paper" :as rnp]
   [declaratie.ref :as ref]
   [declaratie.styles :as styles]
   [declaratie.subs]))

(defn icon []
  (let [stopwatch-visible? (rf/subscribe [:stopwatch-visible?])
        opacity (r/atom ^js (rn/Animated.Value. 1))
        fade-in #(rn/Animated.timing % #js {:toValue 1
                                            :duration 1000
                                            :useNativeDriver true})
        fade-out #(rn/Animated.timing % #js {:toValue 0.5
                                             :duration 1000
                                             :useNativeDriver true})]
    (-> (rn/Animated.sequence #js [(fade-out @opacity) (fade-in @opacity)])
        (rn/Animated.loop #js {:iterations -1})
        (.start))
    (fn []
      [:> (.-View rn/Animated)
       {:style {:opacity @opacity}}
       [:> rnp/IconButton
        {:icon "timer"
         :color "white"
         :on-press (fn []
                     ((:navigate @ref/navigation) "activities-stack")
                     (rf/dispatch [:set-stopwatch-visible true]))}]])))
