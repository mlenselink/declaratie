(ns declaratie.drawer
  (:require
   [goog.object :as gobj]
   ;; [shadow.react-native :refer (render-root)]
   [reagent.core :as r]
   [re-frame.core :as rf]
   ["react" :as react]
   ["react-native" :as rn]
   ["@react-navigation/drawer" :refer (DrawerContentScrollView)]
   ["react-native-calendars" :refer (Calendar LocaleConfig)]
   ["react-native-paper" :as rnp]
   [declaratie.events]
   [declaratie.subs]
   [declaratie.styles :as styles]))

(defonce logo (js/require "./img/vvg-logo.png"))

(defn checkbox [id]
  (let [enabled? (rf/subscribe [:calendar-enabled? id])]
    (fn [id]
      [:> (.-Item rnp/Checkbox)
       {:status (if @enabled? "checked" "unchecked")
        :on-press #(rf/dispatch [:toggle-calendar-id id])}])))

(defn switch []
  (let [dark-theme? (rf/subscribe [:dark-theme?])]
    (fn []
      [:> rnp/Switch
       {:value @dark-theme?
        :on-value-change #(rf/dispatch [:toggle-theme])}])))

(defn screen [props]
  (let [calendars (rf/subscribe [:get-calendars])
        personal-name (rf/subscribe [:get-personal-name])
        personal-number (rf/subscribe [:get-personal-number])
        personal-premium (rf/subscribe [:get-personal-premium])
        calls-limit-count (rf/subscribe [:get-calls-limit-count])
        calls-limit-period (rf/subscribe [:get-calls-limit-period])
        limit-count (r/atom (str @calls-limit-count))
        period-number (r/atom (str (first @calls-limit-period)))
        menu-visible? (r/atom false)]
    (fn [props]
      [:> DrawerContentScrollView
       [:> (.-Item rnp/List)
        {:title "Declaratie Vincent van Gogh"
         :description "Folkert van der Beek"
         :left #(r/as-element [:> (.-Image rnp/Avatar)
                               {:source logo}])}]
       [:> (.-Accordion rnp/List)
        {:title "Persoonsgegevens"
         :left #(r/as-element [:> (.-Icon rnp/List) {:icon "account"}])}
        [:> rnp/Subheading
         {:style {:padding-left 12}}
         "Naam"]
        [:> rnp/TextInput
         {:style {:padding-left 0}
          :mode "outlined"
          :dense true
          :value @personal-name
          :on-change-text #(rf/dispatch [:set-personal-name %])}]
        [:> rnp/Subheading
         {:style {:padding-left 12}}
         "Personeelsnummer"]
        [:> rnp/TextInput
         {:style {:padding-left 0}
          :mode "outlined"
          :dense true
          :value @personal-number
          :keyboard-type "number-pad"
          :on-change-text #(rf/dispatch [:set-personal-number %])}]
        [:> rnp/Subheading
         {:style {:padding-left 12}}
         "Vergoeding van toeslag"]
        [:> (.-Group rnp/RadioButton)
         {:value @personal-premium
          :on-value-change #(rf/dispatch [:set-personal-premium %])}
         [:> (.-Item rnp/RadioButton)
          {:label "Uitbetalen"
           :value "pay"}]
         [:> (.-Item rnp/RadioButton)
          {:label "Tijd voor tijd"
           :value "time"}]]]
       [:> (.-Accordion rnp/List)
        {:title "Agenda's"
         :left #(r/as-element [:> (.-Icon rnp/List) {:icon "calendar-account"}])}
        (doall
         (for [cal @calendars]
           (let [{id :id
                  title :title} cal]
             ^{:key id}
             [:> (.-Item rnp/List)
              {:title title
               :left #(r/as-element [checkbox id])
               :on-press #(rf/dispatch [:toggle-calendar-id id])}])))]
       [:> (.-Accordion rnp/List)
        {:title "Oproepgeschiedenis limiet"
         :left #(r/as-element [:> (.-Icon rnp/List) {:icon "phone-log"}])}
        [:> rnp/Subheading
         {:style {:padding-left 12}}
         "Maximum aantal"]
        [:> rnp/TextInput
         {:style {:padding-left 0}
          :keyboard-type "number-pad"
          :mode "outlined"
          :dense true
          :value @limit-count
          :on-change-text #(reset! limit-count %)
          :on-submit-editing (fn [e]
                               (let [text (gobj/getValueByKeys e #js ["nativeEvent" "text"])]
                                 (when-some [s (re-matches #"\d{1,}" text)]
                                   (rf/dispatch [:set-calls-limit-count (js/parseInt s)]))))}]
        [:> rnp/Subheading
         {:style {:padding-left 12}}
         "Maximum periode"]
        [:> rn/View
         {:style {:flex 1
                  :flex-direction "row"
                  :padding-left 0}}
         [:> rnp/TextInput
          {:style {:flex 1
                   :padding-left 0}
           :keyboard-type "number-pad"
           :mode "outlined"
           :dense true
           :value @period-number
           :on-change-text #(reset! period-number %)
           :on-submit-editing (fn [e]
                                (let [text (gobj/getValueByKeys e #js ["nativeEvent" "text"])]
                                  (when-some [s (re-matches #"\d{1,}" text)]
                                    (rf/dispatch [:set-calls-limit-period (js/parseInt s) (second @calls-limit-period)]))))}]
         [:> rnp/Menu
          {:visible @menu-visible?
           :on-dismiss (fn [] (reset! menu-visible? false))
           :anchor (r/as-element (let [unit (case (second @calls-limit-period)
                                              :days "dagen"
                                              :months "maanden"
                                              :years "jaren")]
                                   [:> rnp/Button
                                    {:style {:flex 1
                                             :height 40
                                             :margin-top 4}
                                     :content-style {:flex-direction "row-reverse"}
                                     :mode "outlined"
                                     :icon (if @menu-visible? "menu-up" "menu-down")
                                     :uppercase false
                                     :on-press (fn [] (reset! menu-visible? true))}
                                    unit]))}
          [:> (.-Item rnp/Menu)
           {:title "dagen"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (rf/dispatch [:set-calls-limit-period (first @calls-limit-period) :days]))}]
          [:> (.-Item rnp/Menu)
           {:title "maanden"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (rf/dispatch [:set-calls-limit-period (first @calls-limit-period) :months]))}]
          [:> (.-Item rnp/Menu)
           {:title "jaren"
            :on-press (fn []
                        (reset! menu-visible? false)
                        (rf/dispatch [:set-calls-limit-period (first @calls-limit-period) :years]))}]]]]
       [:> (.-Accordion rnp/List)
        {:title "Thema"
         :left #(r/as-element [:> (.-Icon rnp/List) {:icon "theme-light-dark"}])}
        [:> (.-Item rnp/List)
         {:title "Donker thema"
          :right #(r/as-element [switch])
          :on-press #(rf/dispatch [:toggle-theme])}]]])))
