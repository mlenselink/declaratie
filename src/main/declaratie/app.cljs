(ns declaratie.app
  (:require
   [goog.object :as gobj]
   [reagent.core :as r]
   [re-frame.core :as rf]
   ["react" :as react]
   ["react-native" :as rn]
   ["react-native-gesture-handler"]
   ["@react-navigation/native" :refer (NavigationContainer)]
   ["@react-navigation/drawer" :refer (createDrawerNavigator)]
   ["@react-navigation/stack" :refer (createStackNavigator)]
   ["@react-navigation/material-bottom-tabs" :refer (createMaterialBottomTabNavigator)]
   ["react-native-safe-area-context" :refer (SafeAreaProvider)]
   ["react-native-vector-icons/MaterialCommunityIcons" :default MaterialCommunityIcons]
   ["react-native-paper" :refer (Badge Provider) :rename {Provider PaperProvider}]
   [declaratie.events]
   [declaratie.subs]
   [declaratie.activities :as activities]
   [declaratie.agenda :as agenda]
   [declaratie.background :as background]
   [declaratie.calls :as calls]
   [declaratie.drawer :as drawer]
   [declaratie.edit :as edit]
   [declaratie.ref :as ref]
   [declaratie.styles :as styles]
   [declaratie.table :as table]))

(def drawer (createDrawerNavigator))

(def stack (createStackNavigator))

(def tab (createMaterialBottomTabNavigator))

;; Navigators

(defn activities-stack []
  [:> (.-Navigator stack)
   {:initial-route-name "activities"
    :screen-options {:header-shown false}}
   [:> (.-Screen stack)
    {:name "activities"
     :component #(r/as-element [activities/screen %])
     :options {:header-title "Activiteiten"}}]
   [:> (.-Screen stack)
    {:name "edit"
     :component #(r/as-element [edit/screen %])
     :options {:header-title "Activiteit toevoegen"
               :presentation "transparentModal"}}]])

(defn tab-navigator []
  [:> (.-Navigator tab)
   {:initial-route-name "agenda"
    :shifting false}
   [:> (.-Screen tab)
    {:name "agenda"
     :component #(r/as-element [agenda/screen %])
     :options {:tab-bar-label "Agenda"
               :tab-bar-icon #(r/as-element [:> MaterialCommunityIcons
                                             {:name "calendar"
                                              :color (gobj/get % "color")
                                              :size 24}])}}]
   [:> (.-Screen tab)
    {:name "calls"
     :component #(r/as-element [calls/screen %])
     :options {:tab-bar-label "Oproepen"
               :tab-bar-icon #(r/as-element [:> MaterialCommunityIcons
                                             {:name "phone-log"
                                              :color (gobj/get % "color")
                                              :size 24}])}}]
   [:> (.-Screen tab)
    {:name "activities-stack"
     :component #(r/as-element [activities-stack])
     :options {:tab-bar-label "Activiteiten"
               :tab-bar-icon #(r/as-element [:> MaterialCommunityIcons
                                             {:name "calendar-clock"
                                              :color (gobj/get % "color")
                                              :size 24}])}}]
   [:> (.-Screen tab)
    {:name "table"
     :component #(r/as-element [table/screen %])
     :options {:tab-bar-label "Tabel"
               :tab-bar-icon #(r/as-element [:> MaterialCommunityIcons
                                             {:name "timetable"
                                              :color (gobj/get % "color")
                                              :size 24}])}}]])

(defn drawer-navigator []
  [:> (.-Navigator drawer)
   {:initial-route-name "tab"
    :screen-options {:header-shown false}
    :drawer-content #(r/as-element [drawer/screen %])}
   [:> (.-Screen drawer)
    {:name "tab"
     :component #(r/as-element [tab-navigator])}]])

(defn approot []
  (let [dark-theme? (rf/subscribe [:dark-theme?])]
    (fn []
      [:> SafeAreaProvider
       [:> PaperProvider {:theme (if @dark-theme? styles/dark-theme styles/default-theme)}
        [:> NavigationContainer
         {:ref #(reset! ref/navigation (js->clj % :keywordize-keys true))
          :theme (if @dark-theme? styles/dark-theme styles/default-theme)}
         [drawer-navigator]]]])))

(defn start
  {:dev/after-load true}
  []
  (-> (.requestMultiple rn/PermissionsAndroid #js [rn/PermissionsAndroid.PERMISSIONS.READ_CALENDAR
                                                   rn/PermissionsAndroid.PERMISSIONS.READ_CALL_LOG
                                                   rn/PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE])
      (.then (fn [permissions]
               ;; Initiate boot process
               (rf/dispatch-sync [:boot])
               ;; Initialize BackgroundFetch ONLY ONCE when component mounts
               (background/init-background-fetch))))
  (.registerComponent rn/AppRegistry "declaratie" #(r/reactify-component approot)))

(defn init []
  (start))
