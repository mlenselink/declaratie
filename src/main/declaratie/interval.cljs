(ns declaratie.interval
  (:require [tick.core :as t]
            [tick.alpha.interval :as t.i]
            [declaratie.time :as time]))

(defn event->interval
  "Return start and end of the event."
  [event]
  ;; Events during all day are given UTC-based start and end dates at midnight,
  ;; thus ignoring time zone.
  (let [all-day? (:allDay event)
        recurrent? (if (:recurrence event) true false)
        start-date (:startDate event)
        end-date (:endDate event)
        start (if all-day?
                (-> start-date
                    (t/instant)
                    (t/date)
                    (t.i/bounds)
                    (t/beginning)
                    (t/instant))
                (-> start-date
                    (t/instant)))
        end (if (:allDay event)
              (-> end-date
                  (t/instant)
                  (t/date)
                  (t.i/bounds)
                  (t/beginning)
                  (t/instant))
              (-> end-date
                  (t/instant)))]
    {:id (:id event)
     :title (:title event)
     :recurrent? recurrent?
     :all-day? all-day?
     :start start
     :end end}))

(defn call->interval
  "Return the interval of the call."
  [call]
  (let [start (js/parseInt (:timestamp call))
        duration (* 1000 (js/parseInt (:duration call)))
        end (+ start duration)]
    (-> call
        (assoc :id (:timestamp call))
        (assoc :start (time/timestamp->instant start))
        (assoc :end (time/timestamp->instant end)))))

(defn round-off-by-quarter
  "Round interval of by quarters."
  [interval]
  (let [start-timestamp (-> interval
                            (:start)
                            (time/instant->timestamp)
                            (time/round-off-by 15 ))
        end-timestamp (-> interval
                          (:end)
                          (time/instant->timestamp)
                          (time/round-off-by 15))]
    (-> interval
        (assoc :start (time/timestamp->instant start-timestamp))
        (assoc :end (time/timestamp->instant end-timestamp)))))

(defn round-up-to-half-hour
  "Round interval to half hours."
  [interval]
  (let [start-timestamp (-> interval
                            (:start)
                            (time/instant->timestamp))
        end-timestamp (-> interval
                          (:end)
                          (time/instant->timestamp)
                          (- start-timestamp) ; duration in milliseconds
                          (time/round-up-to 30) ; duration rounded to half hours
                          (+ start-timestamp))]
    (-> interval
        (assoc :start (time/timestamp->instant start-timestamp))
        (assoc :end (time/timestamp->instant end-timestamp)))))

(defn during-interval?
  "Returns true if interval `x` overlaps interval `y`."
  [x y]
  (let [{x-start :start
         x-end :end} x
        {y-start :start
         y-end :end} y]
    (or (< y-start x-start y-end)
        (< y-start x-end y-end)
        (= x-start y-start)
        (= x-end y-end))))

(defn merge-intervals
  "Merge overlapping intervals in `coll` and return only mutually exclusive intervals."
  [coll]
  (loop [intervals (sort-by (juxt :start :end) coll)
         previous-start nil
         previous-end nil
         result []]
    (if (empty? intervals) ; done
      result
      (let [interval (first intervals)
            start (:start interval)
            end (:end interval)]
        (cond
          ;; empty interval
          (= start end)
          (recur (rest intervals)
                 previous-start
                 previous-end
                 result)
          ;; interval after previous interval
          (or (nil? previous-start)
              (t/> start previous-end))
          (recur (rest intervals)
                 start
                 end
                 (conj result {:start start :end end}))
          ;; interval overlaps or immediately after previous interval
          (t/<= start previous-end)
          (recur (rest intervals)
                 start
                 end
                 (-> result
                     pop
                     (conj {:start previous-start
                            :end end}))))))))

(defn split-intervals
  "Split intervals in `coll` into days, bounded by midnight. Return a vector of intervals."
  [coll]
  (loop [intervals (sort-by (juxt :start :end) coll)
         result []]
    (if (empty? intervals) ; done
      result
      (let [interval (first intervals)
            start (:start interval)
            end (:end interval)
            midnight (-> start
                         (t/date)
                         (t.i/bounds)
                         (t/end)
                         (t/instant))]
        (if (t/> end midnight)
          (recur (conj (rest intervals) {:start midnight :end end})
                 (conj result {:start start :end midnight}))
          (recur (rest intervals)
                 (conj result {:start start :end end})))))))

(defn subtract-interval
  "Subtracts the intervals `y` from interval `x` and returns a vector with the
  resulting intervals."
  ([x y]
   (cond
     ;; interval x is empty
     (= (:start x) (:end x))
     []
     ;; interval y is empty
     (= (:start y) (:end y))
     [{:start (:start x)
       :end (:end x)}]
     ;; interval y is the same as interval x
     (and (= (:start y) (:start x))
          (= (:end y) (:end x)))
     []
     ;; interval y precedes interval x
     (t/<= (:end y) (:start x))
     [{:start (:start x)
       :end (:end x)}]
     ;; interval y starts interval x
     (and (= (:start y) (:start x))
          (t/< (:end y) (:end x)))
     [{:start (:end y)
       :end (:end x)}]
     ;; interval y overlaps interval x
     (t/< (:start y) (:start x) (:end y) (:end x))
     [{:start (:end y)
       :end (:end x)}]
     ;; interval y contains interval x
     (t/< (:start y) (:start x) (:end x) (:end y))
     []
     ;; interval y is during interval x
     (t/< (:start x) (:start y) (:end y) (:end x))
     [{:start (:start x)
       :end (:start y)}
      {:start (:end y)
       :end (:end x)}]
     ;; interval y finishes interval x
     (and (t/< (:start x) (:start y))
          (= (:end x) (:end y)))
     [{:start (:start x)
       :end (:start y)}]
     ;; interval y is overlapped by interval x
     (t/< (:start x) (:start y) (:end x) (:end y))
     [{:start (:start x)
       :end (:start y)}]
     ;; interval y is preceded by interval x
     (t/<= (:end x) (:start y))
     [{:start (:start x)
       :end (:end x)}]))
  ([x y & more]
   (loop [intervals more
          result (subtract-interval x y)]
    (if (empty? intervals) ; done
      result
      (let [interval (first intervals)]
        (recur (rest intervals)
               (->> result
                    (map #(subtract-interval % interval))
                    (apply concat)
                    (into []))))))))
