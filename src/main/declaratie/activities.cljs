(ns declaratie.activities
  (:require
   [goog.object :as gobj]
   [goog.string :as gstring]
   [goog.string.format]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [tick.core :as t]
   ["react-native" :as rn]
   ["react-native-paper" :as rnp]
   ["react-native-vector-icons/MaterialCommunityIcons" :default MaterialCommunityIcons]
   ["react-native-safe-area-context" :refer (SafeAreaView)]
   ["@react-navigation/native" :refer (DrawerActions)]
   ["@react-native-community/datetimepicker" :default DateTimePicker]
   [declaratie.ref :as ref]
   [declaratie.stopwatch :as stopwatch]
   [declaratie.styles :as styles]
   [declaratie.time :as time]
   [declaratie.subs]))

(def message (r/atom nil))

(defn header []
  (let [filter-menu-visible? (r/atom false)
        activity-filter (rf/subscribe [:get-activity-filter])
        running? (rf/subscribe [:stopwatch-running?])]
    (fn []
      [:> (.-Header rnp/Appbar)
       [:> (.-Action rnp/Appbar) {:icon "menu"
                                  :on-press #((:dispatch @ref/navigation) (.toggleDrawer DrawerActions))}]
       [:> (.-Content rnp/Appbar)
        {:title "Activiteiten"}]
       [:> rnp/Menu
        {:anchor (r/as-element [:> rnp/IconButton
                                {:icon "filter"
                                 :color "white"
                                 :on-press #(swap! filter-menu-visible? not)}])
         :visible  @filter-menu-visible?
         :on-dismiss #(reset! filter-menu-visible? false)}
        [:> (.-Group rnp/RadioButton)
         {:on-value-change (fn [value]
                             (rf/dispatch [:set-activity-filter value])
                             (reset! filter-menu-visible? false))
          :value @activity-filter}
         [:> (.-Item rnp/RadioButton)
          {:label "Alle"
           :value false}]
         [:> (.-Item rnp/RadioButton)
          {:label "Geselecteerd"
           :value true}]]]
       (if @running?
         [stopwatch/icon]
         [:> (.-Action rnp/Appbar)
          {:icon "timer"
           :on-press #(rf/dispatch [:toggle-stopwatch-visible])}])])))

(defn menu [key]
  (let [context-menu-visible? (r/atom false)]
    (fn [key]
      [:> rnp/Menu
       {:visible @context-menu-visible?
        :on-dismiss #(reset! context-menu-visible? false)
        :anchor (r/as-element [:> rnp/IconButton
                               {:icon "dots-vertical"
                                :on-press #(swap! context-menu-visible? not)}])}
       [:> (.-Item rnp/Menu)
        {:title "Wijzigen"
         :icon "pencil"
         :on-press (fn []
                     ((:navigate @ref/navigation)
                      "edit"
                      #js {:key key})
                     (reset! context-menu-visible? false))}]
       [:> (.-Item rnp/Menu)
        {:title "Verwijderen"
         :icon "delete"
         :on-press (fn []
                     (rf/dispatch [:remove-activity key])
                     (reset! context-menu-visible? false))}]])))

(defn message-bar []
  [:> rnp/Snackbar
   {:visible @message
    :on-dismiss #(reset! message nil)}
   @message])

(defn error-bar []
  (let [error-message (rf/subscribe [:get-error-message])]
    (fn []
      [:> rnp/Snackbar
       {:visible (not (empty? @error-message))
        :on-dismiss #(rf/dispatch [:set-error-message ""])}
       @error-message])))

(defn stopwatch []
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        running? (rf/subscribe [:stopwatch-running?])
        start (rf/subscribe [:get-stopwatch-start])
        end (rf/subscribe [:get-stopwatch-end])]
    (fn []
      ;; Calculate interval every 1000ms
      (js/setInterval #(when @running? (rf/dispatch [:update-stopwatch])) 1000)
      [:> rnp/Card
       [:> (.-Content rnp/Card)
        {:style {:align-items "center"}}
        [:> rnp/TouchableRipple
         {:on-press (fn [e]
                      (when-not @running?
                        (rf/dispatch [:init-stopwatch]))
                      (rf/dispatch [:toggle-stopwatch]))
          :on-long-press (fn [e]
                           (rf/dispatch [:reset-stopwatch]))}
         (let [duration (if (and @start @end)
                          (t/duration {:tick/beginning @start
	                               :tick/end @end})
                          (t/new-duration 0 :seconds) )
               hours (t/hours duration)
               minutes (t/minutes (t/- duration
                                       (t/new-duration hours :hours)))
               seconds (t/seconds (t/- duration
                                       (t/new-duration minutes :minutes)
                                       (t/new-duration hours :hours)))]
           [:> rn/View
            {:style (styles/stopwatch-border @dark-theme?)}
            [:> rn/View
             {:style (styles/stopwatch-timer @dark-theme?)}
             [:> rnp/Headline
              {:style (styles/stopwatch-text @dark-theme?)}
              (gstring/format "%01d" hours)]
             [:> rnp/Headline
              {:style (styles/stopwatch-text @dark-theme?)}
              (gstring/format "%02d" minutes)]
             [:> rnp/Subheading
              {:style (styles/stopwatch-text @dark-theme?)}
              (gstring/format "%02d" seconds)]]])]
        [:> (.-Actions rnp/Card)
         [:> rnp/IconButton
          {:icon "restart"
           :size 32
           :on-press #(rf/dispatch [:reset-stopwatch])}]
         [:> rnp/IconButton
          {:icon (if @running? "stop-circle" "play-circle")
           :color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :error])
           :size 64
           :on-press (fn []
                       (when-not @running?
                         (rf/dispatch [:init-stopwatch]))
                       (rf/dispatch [:toggle-stopwatch]))}]
         [:> rnp/IconButton
          {:icon "content-save"
           :disabled (not (and @start @end))
           :size 32
           :on-press (fn []
                       (rf/dispatch [:add-activity @start @end])
                       (rf/dispatch [:reset-stopwatch])
                       (reset! message "Activiteit opgeslagen"))}]]]])))

(defn checkbox [id]
  (let [selected? (rf/subscribe [:activity-selected? id])]
    (fn [id]
      [:> rnp/Checkbox {:status (if @selected? "checked" "unchecked")
                        :on-press #(rf/dispatch [:toggle-activity id])}])))

(defn render-item [props]
  (let [{{:keys [id start end]} :item
         index :index
         separators :separators} props]
    [:> (.-Item rnp/List)
     {:title (if (= (t/date start) (t/date end))
               (time/format-date start "d MMM yyyy")
               (str (time/format-date start "dd-MM-yyyy")
                    " - "
                    (time/format-date end "dd-MM-yyyy")))
      :description (str (time/format-date start "HH:mm")
                        "-"
                        (time/format-date end "HH:mm"))
      :description-number-of-lines 1
      :left #(r/as-element [checkbox id])
      :right #(r/as-element [menu id])}]))

(defn activities []
  (let [activities (rf/subscribe [:get-activities])]
    (fn []
      [:> rn/FlatList
       {:data (clj->js @activities)
        :key-extractor (fn [item index] (gobj/get item "id"))
        :render-item #(r/as-element [render-item (js->clj % :keywordize-keys true)])
        :ItemSeparatorComponent #(r/as-element [:> rnp/Divider])}])))

(defn fab []
  (let [open? (r/atom false)]
    (fn []
      [:> (.-Group rnp/FAB)
       {:style {:position "absolute"
                :margin 16
                :right 0
                :bottom 0}
        :open @open?
        :icon "plus"
        :actions [{:icon "calendar-blank"
                   :label "Toevoegen"
                   ;; reagent doesn't convert CamelCase to kebab-case in attributes nested in a vector
                   :onPress (fn []
                              (swap! open? not)
                              ((:navigate @ref/navigation) "edit"))}
                  {:icon "timer"
                   :label "Timer"
                   :onPress (fn []
                              (swap! open? not)
                              (rf/dispatch [:set-stopwatch-visible true]))}]
        ;; onStateChange is required, but not needed because the state is updated is updated on press
        :on-state-change (fn [open?] nil)
        :on-press #(swap! open? not)}])))

(defn screen [props]
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        loading? (rf/subscribe [:loading?])
        stopwatch-visible? (rf/subscribe [:stopwatch-visible?])
        top (-> (.get rn/Dimensions "window")
                (.-height)
                (- 48) ; size of large indicator
                (/ 2))
        left (-> (.get rn/Dimensions "window")
                 (.-width)
                 (- 48) ; size of large indicator
                 (/ 2))]
    (fn [props]
      [:> SafeAreaView
       {:style {:flex 1}}
       [:> rn/StatusBar
        {:background-color (get-in (if @dark-theme? styles/dark-theme styles/default-theme) [:colors :primary])
         :status-bar-style (if @dark-theme? "dark-content" "light-content")}]
       [header]
       (if @stopwatch-visible?
         [:> rnp/Surface
          {:style {:flex-basis "auto"
                   :flex-grow 0}
           :elevation 2}
          [stopwatch]])
       [:> rn/View
        {:style {:flex-basis "auto"
                 :flex-grow 1}}
        [activities]]
       [fab]
       [message-bar]
       [error-bar]
       (when @loading?
         [:> rnp/ActivityIndicator
          {:style {:position "absolute"
                   :top top
                   :left left}
           :size "large"}])])))
