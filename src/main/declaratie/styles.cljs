(ns declaratie.styles
  (:require
   ["react-native" :as rn]
   ["@react-navigation/native" :refer (DarkTheme DefaultTheme) :rename {DarkTheme NavigationDarkTheme DefaultTheme NavigationDefaultTheme}]
   ["react-native-paper" :refer (DarkTheme DefaultTheme) :rename {DarkTheme PaperDarkTheme DefaultTheme PaperDefaultTheme}]))

(defn deep-merge [a & maps]
  (if (map? a)
    (apply merge-with deep-merge a maps)
    (apply merge-with deep-merge maps)))

;; Vincent van Gogh house style
(def pantone-3125 "#00aacd")
(def pantone-397 "#c8d300")
(def pantone-110 "#ffcc00")
(def pantone-3145 "#007399")
(def pantone-227 "#c3006b")
(def pantone-187 "#c10b25")
(def pantone-3105 "#94d3e6")

(def colors {:primary pantone-3125
             :notification pantone-397
             :error pantone-187
             :accent pantone-3105})

(def combined-default-theme (deep-merge (js->clj NavigationDefaultTheme :keywordize-keys true)
                                        (js->clj PaperDefaultTheme :keywordize-keys true)))

(def default-theme (update combined-default-theme :colors merge colors))

(def combined-dark-theme (deep-merge (js->clj NavigationDarkTheme :keywordize-keys true)
                                     (js->clj PaperDarkTheme :keywordize-keys true)))

(def dark-theme (update combined-dark-theme :colors merge colors))

(defn statusbar-color
  [dark-theme?]
  (let [theme (if dark-theme? dark-theme default-theme)]
    (get-in theme [:colors :primary])))

(defn calendar
  [dark-theme?]
  (let [theme (if dark-theme? dark-theme default-theme)]
    {:backgroundColor (get-in theme [:colors :background])
     :calendarBackground (get-in theme [:colors :background])
     :textSectionTitleColor (get-in theme [:colors :placeholder])
     :textSectionTitleDisabledColor (get-in theme [:colors :disabled])
     :todayTextColor (get-in theme [:colors :text])
     :dayTextColor (get-in theme [:colors :text])
     :textDisabledColor (get-in theme [:colors :disabled])
     :dotColor (get-in theme [:colors :primary])
     :textDayFontFamily (get-in theme [:fonts :regular :fontFamily])
     :textDayHeaderFontFamily (get-in theme [:fonts :regular :fontFamily])
     :textDayFontWeight (get-in theme [:fonts :regular :fontWeight])
     :textDayHeaderFontWeight (get-in theme [:fonts :regular :fontWeight])
     ;; Override default calendar style
     "stylesheet.calendar.main" {:container {:padding-left 12
                                             :padding-right 12
                                             ;; :padding-bottom 12
                                             :backgroundColor (get-in theme [:colors :background])}
                                 :week {:margin-top 4
                                        :margin-bottom 4
                                        :flex-direction "row"
                                        :justify-content "space-between"}}
     ;; Override default day style
     "stylesheet.day.basic" {:base {:width 40
                                    :height 40
                                    :margin-left 4
                                    :margin-right 4
                                    :align-items "center"}
                             :selected {:backgroundColor (get-in theme [:colors :primary])
                                        :borderRadius 20}
                             :selectedText {:color (get-in theme [:colors :background])}
                             :today {:borderColor (get-in theme [:colors :primary])
                                     :borderRadius 20
                                     :borderWidth 1}}}))

(defn table-container
  [dark-theme?]
  (let [theme (if dark-theme? dark-theme default-theme)]
    {:flex 1
     :background-color (get-in theme [:colors :background])}))

(defn header-row
  [dark-theme?]
  {:min-height 56
   :border-color (if dark-theme?
                   "rgba(255, 255, 255, 0.12)"
                   "rgba(0, 0, 0, 0.12)")
   :border-bottom-width (* (.-hairlineWidth rn/StyleSheet) 2)})

(defn header-text
  [dark-theme?]
  (let [theme (if dark-theme? dark-theme default-theme)]
    {:color (get-in theme [:colors :placeholder])
     :padding-vertical 6
     :padding-horizontal 8}))

(defn table-row
  [dark-theme?]
  {:flex-direction "row"
   :min-height 52
   :border-color (if dark-theme?
                   "rgba(255, 255, 255, 0.12)"
                   "rgba(0, 0, 0, 0.12)")
   :border-bottom-width (.-hairlineWidth rn/StyleSheet)})

(defn table-text
  [dark-theme?]
  (let [theme (if dark-theme? dark-theme default-theme)]
    {:color (get-in theme [:colors :text])
     :padding-vertical 6
     :padding-left 8}))

(defn table-cell
  [{:keys [align-items width]}]
  {:align-items (or align-items "flex-start")
   :width (or width 100)})

(defn icon-color
  [dark-theme?]
  (let [theme (if dark-theme? dark-theme default-theme)]
    (get-in theme [:colors :text])))

(defn stopwatch-border [dark-theme?]
  (let [theme (if dark-theme? dark-theme default-theme)]
    {:border-color (get-in theme [:colors :text])
     :justify-content "center"
     :width 100
     :height 100
     :border-width 4
     :border-radius 50}))

(defn stopwatch-timer [dark-theme?]
  (let [theme (if dark-theme? dark-theme default-theme)]
    {:flex-direction "row"
     :justify-content "center"
     :align-items "baseline"}))

(defn stopwatch-text [dark-theme?]
  (let [theme (if dark-theme? dark-theme default-theme)]
    {:margin 2}))
