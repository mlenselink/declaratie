(ns declaratie.time
  (:require
   [tick.core :as t])
  (:import
   [goog.date Date]
   [goog.date DateTime]
   [goog.i18n DateTimeFormat]
   [goog.i18n DateTimePatterns]
   [goog.i18n DateTimeSymbols]
   [goog.i18n TimeZone]))

(def year-full "yyyy" (.-YEAR_FULL DateTimePatterns))
(def year-month-abbr "MMM y" (.-YEAR_MONTH_ABBR DateTimePatterns))
(def year-month-full "MMMM yyyy" (.-YEAR_MONTH_FULL DateTimePatterns))
(def month-day-abbr "d MMM" (.-MONTH_DAY_ABBR DateTimePatterns))
(def month-day-full "dd MMMM" (.-MONTH_DAY_FULL DateTimePatterns))
(def month-day-short "d-M" (.-MONTH_DAY_SHORT DateTimePatterns))
(def month-day-medium "d MMMM" (.-MONTH_DAY_MEDIUM DateTimePatterns))
(def month-day-year-medium "d MMM y" (.-MONTH_DAY_YEAR_MEDIUM DateTimePatterns))
(def weekday-month-day-medium "EEE d MMM" (.-WEEKDAY_MONTH_DAY_MEDIUM DateTimePatterns))
(def weekday-month-day-year-medium "EEE d MMM y" (.-WEEKDAY_MONTH_DAY_YEAR_MEDIUM DateTimePatterns))
(def day-abbr "d" (.-DAY_ABBR DateTimePatterns))

(def months (js->clj (.-MONTHS DateTimeSymbols)))
(def shortmonths (js->clj (.-SHORTMONTHS DateTimeSymbols)))
(def weekdays (js->clj (.-WEEKDAYS DateTimeSymbols)))
(def shortweekdays (js->clj (.-SHORTWEEKDAYS DateTimeSymbols)))

;; DATEFORMATS: ['EEEE d MMMM y', 'd MMMM y', 'd MMM y', 'dd-MM-y'],
;; TIMEFORMATS: ['HH:mm:ss zzzz', 'HH:mm:ss z', 'HH:mm:ss', 'HH:mm'],
;; DATETIMEFORMATS: ['{0} {1}', '{0} {1}', '{0}, {1}', '{0}, {1}'],

(def date-formats (js->clj (.-DATEFORMATS DateTimeSymbols)))

(def time-formats (js->clj (.-TIMEFORMATS DateTimeSymbols)))

(defn round-up-to
  "Round duration in milliseconds up to minutes."
  [millis minutes]
  (let [ceiling #(.ceil js/Math %)]
    (-> millis
        (/ (* minutes 60 1000))
        (ceiling)
        (* minutes 60 1000))))

(defn round-off-by
  "Round milliseconds off by minutes."
  [millis minutes]
  (let [round #(.round js/Math %)]
    (-> millis
        (/ (* minutes 60 1000))
        (round)
        (* minutes 60 1000))))

(defn timestamp->instant
  "Convert Unix timestamp (in milliseconds) to `tick` instant.
  The timezone is always zero UTC offset."
  [timestamp]
  (-> timestamp
      (t/new-duration :millis)
      t/instant))

(defn instant->timestamp
  "Convert `tick` instant to Unix timestamp (in milliseconds)."
  [instant]
  (let [duration (t/duration {:tick/beginning (t/epoch)
                              :tick/end instant})
        timestamp (t/millis duration)]
    timestamp))

(defn format-date
  [date pattern]
  (let [formatter (DateTimeFormat. pattern)]
    (.format formatter (t/inst date))))
