(ns declaratie.events
  (:require
   [re-frame.core :as rf]
   [day8.re-frame.async-flow-fx :as async-flow-fx]
   [clojure.spec.alpha :as s]
   [cljs.reader :as reader]
   [clojure.set :as set]
   [clojure.string :as str]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]
   ["react-native" :as rn]
   ["react-native-call-log" :as call-logs]
   ["react-native-calendar-events" :default RNCalendarEvents]
   ["@react-native-async-storage/async-storage" :default AsyncStorage]
   [declaratie.db :as db :refer [app-db]]
   [declaratie.interval :as interval]
   [declaratie.time :as time]))

(def storage-key "declaratie:db")

;; Helpers

;; Async flow (See https://github.com/Day8/re-frame-async-flow-fx)

(defn boot-flow
  []
  {:first-dispatch [:load-localstore]
   :rules [{:when :seen? :events :success-load-localstore :dispatch-n '([:init-current-date] [:load-calendars] [:load-calls])}
           {:when :seen? :events :success-load-calendars :dispatch [:init-events]}
           {:when :seen-both? :events [:success-load-events  :success-load-calls] :halt? true}]})

;; Interceptors
;;
;; See https://github.com/Day8/re-frame/blob/master/docs/Interceptors.md
;;

(defn check-and-throw
  "Throw an exception if db doesn't have a valid spec."
  [spec db [event]]
  (when-not (s/valid? spec db)
    (let [explain-data (s/explain-data spec db)]
      (throw (ex-info (str "Spec check after " event " failed: " explain-data) explain-data)))))

(def validate-spec
  (if goog.DEBUG
    (rf/after (partial check-and-throw ::db/app-db))
    []))

;; Effect Handlers

(rf/reg-fx
 :get-localstore
 (fn []
   (-> (.getItem AsyncStorage storage-key)
       (.then #(rf/dispatch [:success-load-localstore (reader/read-string %)]))
       (.catch #(rf/dispatch [:set-error-message (.-message %)])))))

(rf/reg-fx
 :set-localstore
 (fn
   [value]
   (-> (.setItem AsyncStorage storage-key (pr-str value))
       (.catch #(rf/dispatch [:set-error-message (.-message %)])))))

(rf/reg-fx
 :fetch-calendars
 (fn []
   (-> (.check rn/PermissionsAndroid rn/PermissionsAndroid.PERMISSIONS.READ_CALENDAR)
       (.then (fn [granted?]
                (when granted?
                  (-> (.findCalendars RNCalendarEvents)
                      (.then #(rf/dispatch [:success-load-calendars (js->clj % :keywordize-keys true)]))
                      (.catch #(rf/dispatch [:set-error-message %])))))))))

(rf/reg-fx
 :fetch-events
 (fn [{:keys [start end calendars]}]
   (let [start-date (.toISOString (t/inst start))
         end-date (.toISOString (t/inst end))]
     (-> (.check rn/PermissionsAndroid rn/PermissionsAndroid.PERMISSIONS.READ_CALENDAR)
         (.then (fn [granted?]
                  (when granted?
                    (-> (.fetchAllEvents RNCalendarEvents start-date end-date (clj->js (into [] calendars)))
                        (.then #(rf/dispatch [:success-load-events (js->clj % :keywordize-keys true)]))
                        (.catch #(rf/dispatch [:set-error-message %]))))))
         ))))

(rf/reg-fx
 :fetch-calls
 (fn [args]
   (let [{:keys [limit min-timestamp]} args
         filter #js {:minTimestamp min-timestamp}
         init? (zero? min-timestamp)]
     (-> (.check rn/PermissionsAndroid rn/PermissionsAndroid.PERMISSIONS.READ_CALL_LOG)
         (.then (fn [granted?]
                  (when granted?
                    (-> (call-logs/load limit filter)
                        (.then #(rf/dispatch [:success-load-calls (js->clj % :keywordize-keys true) init?]))
                        (.catch #(rf/dispatch [:set-error-message %]))))))))))

;; Coeffect Handlers

(rf/reg-cofx
 :now
 (fn [cofx _]
   (assoc cofx :now (t/instant))))

(rf/reg-cofx
 :uuid
 (fn [cofx _]
   (let [uuid (str (random-uuid))]
     (assoc cofx :uuid uuid))))

;; Event Handlers

(rf/reg-event-fx
 :boot
 validate-spec
 (fn [_ _]
   {:db (assoc app-db :loading? true)
    :async-flow (boot-flow)}))

(rf/reg-event-fx
 :load-localstore
 (fn [{db :db} _]
   {:get-localstore nil
    :db (assoc db :loading? true)}))

(rf/reg-event-fx
 :save-localstore
 (fn [{db :db} _]
   {:set-localstore db
    :db (assoc db :loading? false)}))

(rf/reg-event-db
 :success-load-localstore
 (fn [db [_ value]]
   (-> db
       (merge value)
       (assoc :loading? false))))

(rf/reg-event-fx
 :load-calls
 (fn [cofx [_ init?]]
   (let [db (:db cofx)]
     {:fetch-calls {:limit -1 ;; use -1 for no limit
                    :min-timestamp (if init? 0 (:last-sync db))}})))

(rf/reg-event-fx
 :prune-calls
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (let [db (:db cofx)
         now (:now cofx)
         calls (:calls db)
         ids (:selected-call-ids db)
         limit-count (:calls-limit-count db)
         limit-period (:calls-limit-period db)
         start-date (t/<< (t/date now) (apply t/new-period limit-period))
         pruned-calls (->> calls
                           (take-last limit-count)
                           (filter (fn [call] (t/> (t/date (:start call)) start-date)))
                           (into []))
         pruned-ids (->> pruned-calls
                         (map :id)
                         (remove ids))]
     {:db (-> db
              (assoc :calls pruned-calls)
              (update :selected-call-ids #(apply disj % pruned-ids) ))
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :success-load-calls
 [(rf/inject-cofx :now)]
 (fn [cofx [_ calls init?]]
   (let [db (:db cofx)
         now (:now cofx)
         beg (t/epoch)
         duration (t/duration {:tick/beginning beg
                               :tick/end now})
         ;; number of milliseconds since 1970/01/01
         timestamp (t/millis duration)
         sorted-calls (->> calls
                           (map interval/call->interval)
                           (sort-by :timestamp)
                           (into []))]
     {:db
      (if init?
        (-> db
            (assoc :calls sorted-calls)
            (assoc :last-sync timestamp))
        (-> db
            (update :calls concat sorted-calls)
            (assoc :last-sync timestamp)))
      :fx [[:dispatch [:prune-calls]]]})))

(rf/reg-event-fx
 :load-calendars
 (fn [cofx _]
   (let [db (:db cofx)]
     {:db (assoc db :loading? true)
      :fetch-calendars nil})))

(rf/reg-event-fx
 :success-load-calendars
 (fn [cofx [_ calendars]]
   (let [db (:db cofx)
         ids (->> calendars
                  (map :id)
                  (into #{}))]
     {:db (-> db
              (assoc :calendars calendars)
              (update :calendar-ids #(set/intersection % ids)))
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :toggle-calendar-id
 (fn [cofx [_ id]]
   (let [db (:db cofx)]
     {:db (update db :calendar-ids #(if (contains? %1 %2) (disj %1 %2) (conj %1 %2)) id)
      :fx [[:dispatch [:save-localstore]]
           [:dispatch [:load-events]]]})))

(rf/reg-event-fx
 :set-calendar-bounds
 (fn [cofx [_ bounds]]
   (let [db (:db cofx)
         start (t/beginning bounds)
         end (t/end bounds)
         calendars (:calendar-ids db)]
     {:db (-> db
              (assoc :loading? true)
              (assoc :calendar-bounds bounds))
      :fetch-events {:start start
                     :end end
                     :calendars calendars}})))

(rf/reg-event-fx
 :init-events
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (let [db (:db cofx)
         now (:now cofx)
         bounds (-> now
                    (t/year-month)
                    (t.i/bounds)
                    (t/<< (t/new-period 6 :months))
                    (t.i/extend (t/new-period 11 :months)))
         start (t/beginning bounds)
         end (t/end bounds)
         calendars (:calendar-ids db)]
     {:db (-> db
              (assoc :loading? true)
              (assoc :calendar-bounds bounds))
      :fetch-events {:start start
                     :end end
                     :calendars calendars}})))

(rf/reg-event-fx
 :load-events
 (fn [cofx _]
   (let [db (:db cofx)
         bounds (:calendar-bounds db)
         start (t/beginning bounds)
         end (t/end bounds)
         calendars (:calendar-ids db)]
     {:db (assoc db :loading? true)
      :fetch-events {:start start
                     :end end
                     :calendars calendars}})))

(rf/reg-event-fx
 :success-load-events
 (fn [cofx [_ events]]
   (let [db (:db cofx)
         sorted-events (->> events
                            (map #(select-keys % [:id :title :recurrence :allDay :startDate :endDate]))
                            (map interval/event->interval)
                            (sort-by (juxt :start :end))
                            (into []))]
     {:db (-> db
              (assoc :events sorted-events))
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :toggle-event
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         events (:selected-events db)
         selected? (->> events
                        (some #(and (= (:id %) (:id event))
                                    (= (:title %) (:title event))
                                    (= (:start %) (:start event))
                                    (= (:end %) (:end event))))
                        true?)]
     (if selected?
       {:db (-> db
                (assoc :loading? true)
                (update :selected-events disj event))
        :fx [[:dispatch [:deselect-calls event]]
             [:dispatch [:deselect-activities event]]
             [:dispatch [:save-localstore]]]}
       {:db (-> db
                (assoc :loading? true)
                (update :selected-events conj event))
        :fx [[:dispatch [:select-calls event]]
             [:dispatch [:select-activities event]]
             [:dispatch [:save-localstore]]]}))))

(rf/reg-event-fx
 :select-event
 (fn [cofx [_ event]]
   (let [db (:db cofx)]
     {:db (-> db
              (assoc :loading? true)
              (update :selected-events conj event))
      :fx [[:dispatch [:select-calls event]]
           [:dispatch [:select-activities event]]
           [:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :clear-events
 (fn [cofx _]
   (let [db (:db cofx)]
     {:db (-> db
              (assoc :selected-events #{})
              (assoc :selected-call-ids #{})
              (assoc :selected-activity-ids #{}))
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :select-calls
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         calls (:calls db)
         ids (->> calls
                  (filter #(interval/during-interval? % event))
                  (map :id))]
     {:db (update db :selected-call-ids into ids)})))

(rf/reg-event-fx
 :deselect-calls
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         events (:selected-events db)
         calls (:calls db)
         intervals (apply interval/subtract-interval event events)
         ids (->> calls
                  (filter (fn [call] (some #(interval/during-interval? call %) intervals)))
                  (map :id))]
     {:db (update db :selected-call-ids #(apply disj % ids) )})))

(rf/reg-event-fx
 :toggle-call
 (fn [cofx [_ id]]
   (let [db (:db cofx)
         ids (:selected-call-ids db)
         selected? (contains? ids id)]
     (if selected?
       {:db (-> db
                (assoc :loading? true)
                (update :selected-call-ids disj id))
        :fx [[:dispatch [:save-localstore]]]}
       {:db (-> db
                (assoc :loading? true)
                (update :selected-call-ids conj id))
        :fx [[:dispatch [:save-localstore]]]}))))

(rf/reg-event-fx
 :select-activities
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         activities (:activities db)
         ids (->> activities
                  (filter (fn [[_ activity]] (interval/during-interval? activity event)))
                  (map key))]
     {:db (update db :selected-activity-ids #(apply conj % ids))})))

(rf/reg-event-fx
 :deselect-activities
 (fn [cofx [_ event]]
   (let [db (:db cofx)
         events (:selected-events db)
         activities (:activities db)
         intervals (apply interval/subtract-interval event events)
         ids (->> activities
                  (filter (fn [[_ activity]] (some #(interval/during-interval? activity %) intervals)))
                  (map key))]
     {:db (update db :selected-activity-ids #(apply disj % ids) )})))

(rf/reg-event-fx
 :toggle-activity
 (fn [cofx [_ id]]
   (let [db (:db cofx)
         activities (:selected-activity-ids db)
         selected? (contains? activities id)]
     (if selected?
       {:db (-> db
                (assoc :loading? true)
                (update :selected-activity-ids disj id))
        :fx [[:dispatch [:save-localstore]]]}
       {:db (-> db
                (assoc :loading? true)
                (update :selected-activity-ids conj id))
        :fx [[:dispatch [:save-localstore]]]}))))

(rf/reg-event-db
 :set-query
 (fn [db [_ query]]
   (assoc db :query query)))

(rf/reg-event-fx
 :init-current-date
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (let [db (:db cofx)
         now (:now cofx)
         today (t/date now)]
     {:db (-> db
              (assoc :current-date today))})))

(rf/reg-event-db
 :set-current-date
 (fn [db [_ date]]
   (assoc db :current-date date)))

(rf/reg-event-fx
 :toggle-theme
 (fn [cofx _]
   (let [db (:db cofx)]
     {:db (update db :dark-theme not)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-storage-directory
 (fn [cofx [_ directory]]
   (let [db (:db cofx)]
     {:db (assoc db :storage-directory directory)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :add-activity
 [(rf/inject-cofx :uuid)]
 (fn [cofx [_ start end]]
   (let [db (:db cofx)
         id (:uuid cofx)
         activity {:start start
                   :end end}]
     {:db (assoc-in db [:activities id] activity)
      :fx [[:dispatch [:save-localstore]]
           [:dispatch [:maybe-select-activity id]]]})))

(rf/reg-event-fx
 :edit-activity
 (fn [cofx [_ id start end]]
   (let [db (:db cofx)
         activity {:start start
                   :end end}]
     {:db (assoc-in db [:activities id] activity)
      :fx [[:dispatch [:save-localstore]]
           [:dispatch [:maybe-select-activity id]]]})))

(rf/reg-event-fx
 :remove-activity
 (fn [cofx [_ id]]
   (let [db (:db cofx)]
     {:db (-> db
              (update :activities dissoc id)
              (update :selected-activity-ids disj id))
      :fx [[:dispatch [:save-localstore]]
           [:dispatch [:maybe-select-activity id]]]})))

(rf/reg-event-fx
 :maybe-select-activity
 (fn [cofx [_ id]]
   (let [db (:db cofx)
         events (:selected-events db)
         activity (get-in db [:activities id])
         selected? (->> events
                        (some #(interval/during-interval? activity %))
                        (true?))]
     {:db (update db :selected-activity-ids (if selected? conj disj) id)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :toggle-stopwatch
 (fn [cofx _]
   (let [db (:db cofx)]
     {:db (update db :stopwatch-running? not)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :init-stopwatch
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (let [db (:db cofx)
         now (:now cofx)]
     {:db (assoc-in db [:stopwatch-interval :start] now)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :update-stopwatch
 [(rf/inject-cofx :now)]
 (fn [cofx _]
   (let [db (:db cofx)
         now (:now cofx)]
     {:db (assoc-in db [:stopwatch-interval :end] now)})))

(rf/reg-event-fx
 :reset-stopwatch
 (fn [cofx _]
   (let [db (:db cofx)]
     {:db (-> db
              (assoc :stopwatch-running? false)
              (assoc :stopwatch-interval {:start nil
                                          :end nil}))
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-event-filter
 (fn [cofx [_ event-filter]]
   (let [db (:db cofx)]
     {:db (assoc db :event-filter event-filter)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-call-filter
 (fn [cofx [_ call-filter]]
   (let [db (:db cofx)]
     {:db (assoc db :call-filter call-filter)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-activity-filter
 (fn [cofx [_ activity-filter]]
   (let [db (:db cofx)]
     {:db (assoc db :activity-filter activity-filter)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-db
 :set-calendar-visible
 (fn [db [_ visible?]]
   (-> db
       (assoc :calendar-visible? (if visible? true false)))))

(rf/reg-event-db
 :toggle-calendar-visible
 (fn [db _]
   (update db :calendar-visible? not)))

(rf/reg-event-db
 :set-stopwatch-visible
 (fn [db [_ visible?]]
   (-> db
       (assoc :stopwatch-visible? (if visible? true false)))))

(rf/reg-event-db
 :toggle-stopwatch-visible
 (fn [db _]
   (update db :stopwatch-visible? not)))

(rf/reg-event-fx
 :set-personal-name
 (fn [cofx [_ name]]
   (let [db (:db cofx)]
     {:db (assoc db :personal-name name)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-personal-number
 (fn [cofx [_ number]]
   (let [db (:db cofx)]
     {:db (assoc db :personal-number number)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-personal-premium
 (fn [cofx [_ arrangement]]
   (let [db (:db cofx)]
     {:db (assoc db :personal-premium arrangement)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-calls-limit-count
 (fn [cofx [_ n]]
   (let [db (:db cofx)]
     {:db (assoc db :calls-limit-count n)
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-fx
 :set-calls-limit-period
 (fn [cofx [_ n u]]
   (let [db (:db cofx)]
     {:db (assoc db :calls-limit-period [n u])
      :fx [[:dispatch [:save-localstore]]]})))

(rf/reg-event-db
 :set-loading
 (fn [db [_ loading?]]
   (assoc db :loading? loading?)))

(rf/reg-event-db
 :set-error-message
 (fn [db [_ error]]
   (assoc db :error-message error)))
