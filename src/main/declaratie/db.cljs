(ns declaratie.db
  (:require [clojure.spec.alpha :as s]
            [tick.core :as t]))

;; spec of app-db
(s/def ::current-date t/date?)
(s/def ::last-sync int?)
(s/def ::calendars vector?)
(s/def ::calendar-ids set?)
(s/def ::events vector?)
(s/def ::calls vector?)
(s/def ::activities map?)
(s/def ::selected-events set?)
(s/def ::selected-call-ids set?)
(s/def ::selected-activity-ids set?)
(s/def ::event-filter boolean?)
(s/def ::call-filter boolean?)
(s/def ::activity-filter boolean?)
(s/def ::dark-theme boolean?)
(s/def ::storage-directory (s/nilable string?))
(s/def ::calendar-visible? boolean?)
(s/def ::stopwatch-visible? boolean?)
(s/def ::stopwatch-running? boolean?)
(s/def ::stopwatch-interval map?)
(s/def ::personal-name string?)
(s/def ::personal-number string?)
(s/def ::personal-premium string?)
(s/def ::personal-data map?)
(s/def ::calls-limit-count int?)
(def unit? #{:days :months :years})
(s/def ::calls-limit-period (s/tuple int? unit?))
(s/def ::error-message (s/nilable string?))

(s/def ::app-db
  (s/keys :req-un [::current-date ::last-sync ::calendars ::calendar-ids ::events ::calls ::activities ::selected-events ::selected-call-ids ::selected-activity-ids ::calls ::event-filter ::call-filter ::activity-filter ::dark-theme ::storage-directory ::calendar-visible? ::stopwatch-visible? ::stopwatch-running? ::stopwatch-interval ::personal-name ::personal-number ::personal-premium ::calls-limit-count ::calls-limit-period ::error-message]
          :opt-un [::loading? ::query ::calendar-bounds]))

;; initial state of app-db
(def app-db {:last-sync 0
             :calendars []
             :calendar-ids #{}
             :events []
             :calls []
             :activities {}
             :selected-events #{}
             :selected-call-ids #{}
             :selected-activity-ids #{}
             :event-filter false
             :call-filter false
             :activity-filter false
             :current-date (t/today)
             :dark-theme false
             :storage-directory nil
             :calendar-visible? true
             :stopwatch-visible? true
             :stopwatch-running? false
             :stopwatch-interval {:start nil
                                  :end nil}
             :personal-name ""
             :personal-number ""
             :personal-premium "pay"
             :calls-limit-count 2000
             :calls-limit-period [1 :years]
             :error-message nil})
