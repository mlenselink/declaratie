(ns declaratie.agenda
  (:require
   [goog.object :as gobj]
   [reagent.core :as r]
   [re-frame.core :as rf]
   [clojure.string :as str]
   [tick.core :as t]
   [tick.alpha.interval :as t.i]
   ["react" :as react]
   ["react-native" :as rn]
   ["react-native-bidirectional-infinite-scroll" :refer (FlatList)]
   ["react-native-calendars" :refer (Calendar WeekCalendar LocaleConfig)]
   ["react-native-vector-icons/MaterialCommunityIcons" :default MaterialCommunityIcons]
   ["react-native-paper" :as rnp]
   ["react-native-send-intent" :as rnsi]
   ["react-native-safe-area-context" :refer (SafeAreaView)]
   ["@react-navigation/native" :refer (DrawerActions)]
   [declaratie.events]
   [declaratie.ref :as ref]
   [declaratie.stopwatch :as stopwatch]
   [declaratie.styles :as styles]
   [declaratie.subs]
   [declaratie.time :as time]))

(set! (.. LocaleConfig -locales -nl) (clj->js {:monthNames time/months
                                               :monthNamesShort time/shortmonths
                                               :dayNames time/weekdays
                                               :dayNamesShort time/shortweekdays
                                               :today "Vandaag"}))

(set! (.. LocaleConfig -defaultLocale) "nl")

(def searchbar-visible? (r/atom false))

(def context-menu-visible? (r/atom false))

(def context-menu-coordinates (r/atom {:x 0 :y 0}))

(def dialog-visible? (r/atom false))

(def viewable-items (atom #{}))

(def viewability-config #js {:minimumViewTime 500
                             ;; Nothing is considered viewable until the user
                             ;; scrolls or recordInteraction is called after
                             ;; render.
                             :waitForInteraction true
                             ;; At least one of the
                             ;; viewAreaCoveragePercentThreshold or
                             ;; itemVisiblePercentThreshold is required.
                             :itemVisiblePercentThreshold 75})

(defn get-index
  "Return the index of `events` of which the startDate is on, just after or just before `date`."
  [date events]
  (let [after (fn [[index event]]
                (t/>= (t/date (:start event)) date))
        indexed (map-indexed (fn [index event] [index event]) events)]
    (or (ffirst (filter after indexed))
        (first (last indexed)))))

(defn on-month-change
  "Callback function on changing the calendar month."
  [props]
  (let [day (gobj/get props "dateString")
        date (t/date day)
        year-month (t/year-month date)
        month-bounds (t.i/bounds year-month)
        current-bounds (rf/subscribe [:get-calendar-bounds])
        events (rf/subscribe [:query-events])]
    (cond
      (< date (t/beginning @current-bounds)) (let [new-bounds (t.i/new-interval (t/beginning month-bounds) (t/end @current-bounds))]
                                               (rf/dispatch [:set-calendar-bounds new-bounds]))
      (> date (t/end @current-bounds)) (let [new-bounds (t.i/new-interval (t/beginning @current-bounds) (t/end month-bounds))]
                                         (rf/dispatch [:set-calendar-bounds new-bounds])))
    (rf/dispatch [:set-current-date date])))

(defn get-item-layout
  [data index]
  (let [item-height 68.5 ; list item of 68.5px
        separator-height 0.5 ; divider of 0.5px
        length (+ item-height separator-height)] ; height of the row
    #js {:length length
         :offset (* index length) ; distance from the top of the first row to the top of the row at index
         :index index}))

(defn on-day-press
  "Callback function on pressing a day in the calendar."
  [date]
  (let [events (rf/subscribe [:query-events])
        day (gobj/get date "dateString")
        date (t/date day)
        pred (fn [[index event]]
               (t/>= (t/date (:start event))
                     (t/date day)))]
    (rf/dispatch [:set-current-date date])
    (when-not (empty? @events)
      (.scrollToIndex ^js/FlatList @ref/calendar-list #js {:index (get-index date @events)}))))

(defn context-menu []
  [:> rnp/Menu
   {:visible @context-menu-visible?
    :on-dismiss #(reset! context-menu-visible? false)
    :anchor (clj->js @context-menu-coordinates)}
   [:> (.-Item rnp/Menu)
    {:title "Alles selecteren"
     :on-press (fn []
                 (reset! context-menu-visible? false)
                 (doseq [id @viewable-items]
                   (rf/dispatch [:select-event-id id])))}]
   [:> (.-Item rnp/Menu)
    {:title "Niets selecteren"
     :on-press (fn []
                 (reset! context-menu-visible? false)
                 (reset! dialog-visible? true))}]])

(defn dialog []
  [:> rnp/Portal
   [:> rnp/Dialog
    {:visible @dialog-visible?}
    [:> (.-Title rnp/Dialog) "Niets selecteren?"]
    [:> (.-Content rnp/Dialog)
     [:> rnp/Paragraph
      "Alle activiteiten en gesprekken zullen ook gedeselecteerd worden. Er zal niets worden verwijderd."]]
    [:> (.-Actions rnp/Dialog)
     [:> rnp/Button
      {:on-press #(reset! dialog-visible? false)}
      "Annuleren"]
     [:> rnp/Button
      {:on-press (fn []
                   (rf/dispatch [:clear-events])
                   (reset! dialog-visible? false))}
      "OK"]]]])

(defn checkbox [event]
  (let [selected? (rf/subscribe [:event-selected? event])]
    (fn [event]
      [:> rnp/Checkbox
       {:status (if @selected? "checked" "unchecked")
        :on-press #(rf/dispatch [:toggle-event event])}])))

(defn render-item [props]
  (let [{{id :id
          title :title
          all-day? :all-day?
          recurrent? :recurrent?
          start :start
          end :end
          :as event} :item
         index :index
         separators :separators} props
        events (rf/subscribe [:query-events])
        current-date (-> start
                         t/date)
        previous-date (some-> @events
                              (nth (- index 1) nil)
                              :start
                              t/date)]
    (fn [props]
      [:> rn/View
       {:style {:flex 1
                :flex-direction "row"}}
       [:> rn/View
        {:style {:flex 1
                 :align-items "center"
                 :padding-horizontal 8}}
        (when-not (= current-date previous-date)
          [:<>
           [:> rnp/Caption
            (-> start
                t/day-of-week
                t/int
                (rem 7) ; Sunday is at index 0 instead of 7
                time/shortweekdays
                str/capitalize)]
           [:> rnp/Headline
            (-> start
                t/day-of-month
                str)]])]
       [:> (.-Item rnp/List)
        {:style {:flex 7}
         :on-press #(rf/dispatch [:toggle-event event])
         :on-long-press (fn [e]
                          (let [x (gobj/getValueByKeys e #js ["nativeEvent" "pageX"])
                                y (gobj/getValueByKeys e #js ["nativeEvent" "pageY"])]
                            (reset! context-menu-coordinates {:x x :y y})
                            (reset! context-menu-visible? true)))
         :title title
         :description (if all-day?
                        (str (time/format-date start "dd-MM-yyyy")
                             " "
                             "Hele dag")
                        (str (time/format-date start "dd-MM-yyyy")
                             " "
                             (time/format-date start "HH:mm")
                             " - "
                             (time/format-date end "HH:mm")))
         :description-number-of-lines 1
         :right (fn [props] (r/as-element [:<>
                                           (when all-day?
                                             [:> (.-Icon rnp/List)
                                              {:icon "cached"}])
                                           [checkbox event]]))}]])))

(defn fetch-previous-events []
  (let [current-bounds (rf/subscribe [:get-calendar-bounds])
        new-bounds (t.i/new-interval
                    (t/<< (t/beginning @current-bounds) (t/new-period 6 :months))
                    (t/end @current-bounds))]
    (-> (js/Promise.resolve (rf/dispatch [:set-calendar-bounds new-bounds]))
        (.catch #(rf/dispatch [:set-error-message %])))))

(defn fetch-next-events []
  (let [current-bounds (rf/subscribe [:get-calendar-bounds])
        new-bounds (t.i/new-interval
                    (t/beginning @current-bounds)
                    (t/>> (t/end @current-bounds) (t/new-period 6 :months)))]
    (-> (js/Promise.resolve (rf/dispatch [:set-calendar-bounds new-bounds]))
        (.catch #(rf/dispatch [:set-error-message %])))))

(defn date->month
  "Get total month count for `date`."
  [date]
  (let [year (t/int (t/year date))
        month (t/int (t/month date))]
    (+ month (* 12 year))))

(defn diff-months
  "Get difference in months for dates `d1` and `d2`."
  [d1 d2]
  (let [m1 (date->month d1)
        m2 (date->month d2)]
    (- m2 m1)))

(defn on-viewable-items-changed [props]
  (let [calendar-visible? (rf/subscribe [:calendar-visible?])
        current-date (rf/subscribe [:get-current-date])
        items (-> props
                  (js->clj :keywordize-keys true)
                  :viewableItems)
        ids (->> items
                 (map (comp :id :item))
                 (into #{}))
        date (if-some [start (get-in items [0 :item :start])]
               (t/date start))]
    (reset! viewable-items ids)
    (when date
      (rf/dispatch [:set-current-date date])
      (when @calendar-visible?
        (let [diff (diff-months @current-date date)]
          (when-not (zero? diff)
            (.addMonth ^js/Calendar @ref/calendar diff)))))))

(defn searchbar []
  (let [query (r/atom "")
        calendar-visible? @(rf/subscribe [:calendar-visible?])]
    (fn []
      (rf/dispatch [:set-calendar-visible false])
      [:> rnp/Searchbar
       {:auto-capitalize "none"
        :placeholder "Zoeken in agenda"
        :value @query
        :on-change-text #(reset! query %)
        :on-end-editing #(rf/dispatch [:set-query @query])
        :icon "arrow-left"
        :on-icon-press (fn []
                         (rf/dispatch [:set-query nil])
                         (reset! searchbar-visible? false)
                         (rf/dispatch [:set-calendar-visible calendar-visible?]))}])))

(defn header []
  (let [current-date (rf/subscribe [:get-current-date])
        calendar-visible? (rf/subscribe [:calendar-visible?])
        filter-menu-visible? (r/atom false)
        event-filter (rf/subscribe [:get-event-filter])
        running? (rf/subscribe [:stopwatch-running?])]
    (fn []
      [:> (.-Header rnp/Appbar)
       [:> (.-Action rnp/Appbar)
        {:icon "menu"
         :on-press #((:dispatch @ref/navigation) (.toggleDrawer DrawerActions))}]
       [:> rnp/Button
        ;; Use the style of Appbar.Content for correct alignment of the header
        ;; children, but leave out the horizontal padding to create enough space
        ;; for the button contents
        {:style {:flex 1
                 :margin-left 8
                 :align-items "center"}
         :content-style {:flex-direction "row-reverse"}
         :uppercase false
         :color "white"
         :icon (if @calendar-visible? "menu-up" "menu-down")
         :on-press #(rf/dispatch [:toggle-calendar-visible])}
        (some-> @current-date
                (t/at "00:00")
                (time/format-date "MMMM yyyy")
                (str/capitalize))]
       [:> (.-Action rnp/Appbar)
        {:icon "magnify"
         :on-press #(reset! searchbar-visible? true)}]
       [:> rnp/Menu
        {:anchor (r/as-element [:> rnp/IconButton
                                {:icon "filter"
                                 :color "white"
                                 :on-press #(swap! filter-menu-visible? not)}])
         :visible  @filter-menu-visible?
         :on-dismiss #(reset! filter-menu-visible? false)}
        [:> (.-Group rnp/RadioButton)
         {:on-value-change (fn [value]
                             (rf/dispatch [:set-event-filter value])
                             (reset! filter-menu-visible? false))
          :value @event-filter}
         [:> (.-Item rnp/RadioButton)
          {:label "Alle"
           :value false}]
         [:> (.-Item rnp/RadioButton)
          {:label "Geselecteerd"
           :value true}]]]
       [:> (.-Action rnp/Appbar)
        {:icon "refresh"
         :on-press #(rf/dispatch [:load-events])}]
       (if @running?
         [stopwatch/icon])])))

(defn error-bar []
  (let [error-message (rf/subscribe [:get-error-message])]
    (fn []
      [:> rnp/Snackbar
       {:visible (not (empty? @error-message))
        :on-dismiss #(rf/dispatch [:set-error-message ""])}
       @error-message])))

(defn calendar []
  (let [loading? (rf/subscribe [:loading?])
        current-date (rf/subscribe [:get-current-date])
        events (rf/subscribe [:get-events])
        marked (rf/subscribe [:get-marked])
        dark-theme? (rf/subscribe [:dark-theme?])]
    (fn []
      [:> Calendar
       {:ref #(reset! ref/calendar %)
        :key @dark-theme? ; hack to re-render the calendar when the theme changes
        :theme (styles/calendar @dark-theme?)
        :marked-dates (clj->js @marked)
        :marking-type "dot" ; date marking style ["dot" "multi-dot" "period" "multi-period" "custom"]
        :current (str (t/today)) ; initially visible month in 'yyyy-MM-dd' format
        :on-day-press on-day-press ; handler which gets executed on day press
        :on-month-change on-month-change ; callback for month change event
        :hide-arrows false ; show month navigation arrows
        ;; Replace default arrows with custom ones
        :render-arrow (fn [direction] (r/as-element [:> MaterialCommunityIcons
                                                     {:name (case direction
                                                              "left" "chevron-left"
                                                              "right" "chevron-right")
                                                      :color (get-in (if dark-theme? styles/dark-theme styles/default-theme) [:colors :primary])
                                                      :size 24}]))
        :on-press-arrow-left #(.addMonth ^js/Calendar @ref/calendar -1)
        :on-press-arrow-right #(.addMonth ^js/Calendar @ref/calendar 1)
        :hide-extra-days false ; show days of other months in month page
        :first-day 1 ; week starts from Monday
        :hide-day-names false ; show day names
        :show-week-numbers false ; show week numbers to the left
        :disable-all-touch-events-for-disabled-days false ; enable touch events for disabled days
        ;; Replace default month and year title with custom one
        :render-header #(r/as-element
                         [:> rnp/Button
                          {:icon "calendar-today"
                           :uppercase false
                           :on-press (fn []
                                       (.addMonth ^js/Calendar @ref/calendar (diff-months @current-date (t/today)))
                                       (when-not (empty? @events)
                                         (.scrollToIndex ^js/FlatList @ref/calendar-list #js {:index (get-index (t/today) @events)}))
                                       (rf/dispatch [:set-current-date (t/today)]))}
                          "Vandaag"])
        :enable-swipe-months true}]))) ; enable swiping between months

;; Define `viewabilityConfig` in the constructor to avoid following error:
;; "Error: Changing viewabilityConfig on the fly is not supported" (see
;; https://github.com/facebook/react-native/issues/17408)
(defn calendar-list []
  (let [events (rf/subscribe [:query-events])
        current-date (rf/subscribe [:get-current-date])]
    (r/create-class
     {:constructor (fn [this props]
                     (set! (.-viewabilityConfig ^js/FlatList this) viewability-config))
      :reagent-render
      (fn []
        [:> FlatList
         {:ref #(reset! ref/calendar-list %)
          :data (clj->js @events)
          :render-item #(r/as-element [render-item (js->clj % :keywordize-keys true)])
          :ItemSeparatorComponent #(r/as-element [:> rnp/Divider])
          :key-extractor (fn [item index]
                           ;; Use a concatenation of id, startdate and enddate as
                           ;; unique key because of duplicate ids in repeating
                           ;; events. If only the index is used, the list isn't
                           ;; updated when new events are loaded.
                           (str (gobj/get item "id")
                                (gobj/get item "start")
                                (gobj/get item "end")))
          :get-item-layout get-item-layout
          :initial-scroll-index (get-index @current-date @events)
          :on-start-reached #(fetch-previous-events)
          :on-end-reached #(fetch-next-events)
          :on-viewable-items-changed on-viewable-items-changed
          :on-start-reached-threshold 72
          :on-end-reached-threshold 72
          :show-default-loading-indicators false}])})))

(defn screen [props]
  (let [dark-theme? (rf/subscribe [:dark-theme?])
        loading? (rf/subscribe [:loading?])
        calendar-visible? (rf/subscribe [:calendar-visible?])
        calendars (rf/subscribe [:get-calendars])
        events (rf/subscribe [:get-all-events])
        top (-> (.get rn/Dimensions "window")
                (.-height)
                (- 48) ; size of large indicator
                (/ 2))
        left (-> (.get rn/Dimensions "window")
                 (.-width)
                 (- 48) ; size of large indicator
                 (/ 2))]
    (fn [props]
      [:> SafeAreaView
       {:style {:flex 1}}
       [:> rn/StatusBar
        {:background-color (styles/statusbar-color @dark-theme?)
         :status-bar-style (if @dark-theme? "dark-content" "light-content")}]
       (if @searchbar-visible?
         [searchbar]
         [header])
       (if @calendar-visible?
         [:> rnp/Surface
          {:style {:flex-basis "auto"
                   :flex-grow 0}
           :elevation 2}
          [calendar]])
       [:> rn/View
        {:style {:flex-basis "auto"
                 :flex-grow 1}}
        (cond
          (empty? @calendars) [:<>
                               [:> rnp/Paragraph "Geen agenda's gevonden"]
                               [:> rn/View {:style {:flex-direction "row"}}
                                [:> rnp/Paragraph "Geef de app toegang tot de agenda in de "]
                                [:> rnp/Button
                                 {:mode "outlined"
                                  :label-style {:font-size 14
                                                :line-height 20
                                                :margin-vertical 2}
                                  :uppercase false
                                  :on-press #(.openSettings rn/Linking)}
                                 "instellingen"]]
                               [:> rn/View {:style {:flex-direction "row"}}
                                [:> rnp/Paragraph "Klik daarna op "]
                                [:> rnp/Button
                                 {:mode "outlined"
                                  :label-style {:font-size 14
                                                :line-height 20
                                                :margin-vertical 2}
                                  :uppercase false
                                  :on-press #(rf/dispatch [:load-events])}
                                 "verversen"]]]
          (empty? @events) [:<>
                            [:> rnp/Paragraph "Geen agenda-afspraken gevonden"]
                            [:> rn/View
                             {:style {:flex-direction "row"}}
                             [:> rnp/Paragraph "Voeg een afspraak toe in de agenda of "]
                             [:> rnp/Button
                              {:mode "outlined"
                               :label-style {:font-size 14
                                             :line-height 20
                                             :margin-vertical 2}
                               :uppercase false
                               :on-press #(rnsi/openSettings "android.settings.ADD_ACCOUNT_SETTINGS")}
                              "voeg een account toe"]]
                            [:> rn/View {:style {:flex-direction "row"}}
                             [:> rnp/Paragraph "Klik daarna op "]
                             [:> rnp/Button
                              {:mode "outlined"
                               :label-style {:font-size 14
                                             :line-height 20
                                             :margin-vertical 2}
                               :uppercase false
                               :on-press #(rf/dispatch [:load-events])}
                              "verversen"]]]
          :else [calendar-list])]
       [context-menu]
       [dialog]
       [error-bar]
       (when @loading?
         [:> rnp/ActivityIndicator
          {:style {:position "absolute"
                   :top top
                   :left left}
           :size "large"}])])))
