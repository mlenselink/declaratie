(ns declaratie.background
  (:require
   [goog.object :as gobj]
   [re-frame.core :as rf]
   [cljs.core.async :refer [go]]
   [cljs.core.async.interop :refer-macros [<p!]]
   ["react-native-call-log" :as call-logs]
   ["@react-native-async-storage/async-storage" :default AsyncStorage]
   ["react-native-background-fetch" :default BackgroundFetch]))

(def fetch-config
  #js {:minimumFetchInterval 15
       :stopOnTerminate false
       :startOnBoot true
       :forceAlarmManager false
       :requiresBatteryNotLow true
       :requiresStorageNotLow true
       :requiresCharging false
       :requiresDeviceIdle false})

(defn callback-fn
  "BackgroundFetch event callback"
  [task-id]
  (rf/dispatch [:load-calls])
  (.finish BackgroundFetch task-id))

(defn timeout-fn
  "BackgroundFetch timeout callback"
  [task-id]
  (.finish BackgroundFetch task-id))

(defn init-background-fetch []
  (go
    (try
      ;; Schedule a periodic task "com.fvdbeek.load-calls" to execute every 15 minutes
      (<p! (.configure BackgroundFetch
                       fetch-config
                       ;; Event callback
                       callback-fn
                       ;; Timeout callback
                       timeout-fn))
      (catch js/Error error (rf/dispatch [:set-error-message (ex-cause error)])))))
