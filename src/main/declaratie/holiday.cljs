(ns declaratie.holiday
  (:require
   ["date-holidays" :as Holidays]))

(defn holiday? [date]
  "Return true if `date` is a public holiday in the Netherlands, else return
  `false`."
  (let [holidays (Holidays. "NL")
        holiday (js->clj (.isHoliday holidays date) :keywordize-keys true)
        public? #(= (:type %) "public")]
    (and holiday
         (->> holiday
              (some public?)
              true?))))
