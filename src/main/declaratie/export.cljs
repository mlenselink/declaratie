(ns declaratie.export
  (:require [re-frame.core :as rf]
            [tick.core :as t]
            [tick.alpha.interval :as t.i]
            ["react-native-fs" :as RNFS]
            ["exceljs" :as exceljs]
            ["rfc4648" :refer (base64)]
            [declaratie.holiday :as holiday]
            [declaratie.time :as time]))

(def xlsx-path
  "Relative path of the xlsx template from the root of the assets folder."
  "xlsx/Declaratieformulier Crisisdienst Medical Office - versie juni 2015.xlsx")

(def worksheet-name
  "Name of the worksheet"
  "Declaratie Crisisdienst")

(def name-ref
  "Cell of the name."
  "C3")

(def number-ref
  "Cell of the number."
  "C4")

(def premium-pay-ref
  "Cell of the premium pay choice."
  "H12")

(def premium-time-ref
  "Cell of the premium time choice."
  "H13")

(def column-start
  "Start column of the table."
  2)

(def row-start
  "Start row of the table."
  20)

(defn load-xlsx [filepath]
  "Load workbook from xlsx in `filepath`.
`filepath` is the relative path to the file from the root of the assets folder."
  (let [workbook (new exceljs/Workbook)]
    ;; read xslx from file
    (-> (RNFS/readFileAssets filepath "base64")
        ;; parse base64 string to buffer
        (.then (fn [contents] (.parse base64 contents)))
        ;; load workbook from buffer
        (.then (fn [buffer] (-> workbook
                                (.-xlsx)
                                (.load buffer)))))))

(defn process-interval
  "Populate cells from a table row."
  [interval]
  (let [{:keys [start end standby? worked?]} interval
        untill-midnight? (not= (t/date start) (t/date end))
        weekday-number (-> start
                           t/date
                           t/day-of-week
                           t/int)
        holiday? (holiday/holiday? (t/date start))
        weekdays? (and (not holiday?)
                       (some #(= weekday-number %) [1 2 3 4 5]))
        weekend? (and (not holiday?)
                      (some #(= weekday-number %) [0 6]))
        date (time/format-date start "dd-MM-yyyy")
        weekday (time/format-date start "EEEE")
        start-hour-standby (when standby? (t/hour start))
        start-minute-standby (when standby? (t/minute start))
        end-hour-standby (when standby? (if untill-midnight? 24 (t/hour end)))
        end-minute-standby (when standby? (t/minute end))
        total-hours-standby (when standby?
                              (-> (t.i/new-interval start end)
                                  (t/duration)
                                  (t/divide (t/new-duration 1 :hours))))
        weekdays-standby (when (and standby? weekdays?) total-hours-standby)
        weekend-standby (when (and standby? weekend?) total-hours-standby)
        holiday-standby (when (and standby? holiday?) total-hours-standby)
        start-hour-worked (when worked? (t/hour start))
        start-minute-worked (when worked? (t/minute start))
        end-hour-worked (when worked? (t/hour end))
        end-minute-worked (when worked? (t/minute end))
        daytime (t.i/new-interval (-> start
                                    (t/date)
                                    (t/at (t/time "06:00"))
                                    (t/instant))
                                (-> start
                                    (t/date)
                                    (t/at (t/time "22:00"))
                                    (t/instant)))
        hours-total (when worked?
                      (-> (t.i/new-interval start end)
                          (t/duration)
                          (t/divide (t/new-duration 1 :hours))))
        hours-daytime (when worked?
                        (some-> (t.i/new-interval start end)
                                (t.i/concur daytime)
                                (t/duration)
                                (t/divide (t/new-duration 1 :hours))))
        premium-50 (or hours-daytime 0)
        premium-100 (- hours-total premium-50)]
    {:date date
     :weekday weekday
     :holiday (if holiday? 1 nil)
     :start-hour-standby (or start-hour-standby nil)
     :start-minute-standby (or start-minute-standby nil)
     :end-hour-standby (or end-hour-standby nil)
     :end-minute-standby (or end-minute-standby nil)
     :total-hours-standby (or total-hours-standby nil)
     :weekdays-standby (or weekdays-standby nil)
     :weekend-standby (or weekend-standby nil)
     :holiday-standby (or holiday-standby nil)
     :start-hour-worked (or start-hour-worked nil)
     :start-minute-worked (or start-minute-worked nil)
     :end-hour-worked (or end-hour-worked nil)
     :end-minute-worked (or end-minute-worked nil)
     :premium-50 premium-50
     :premium-100 premium-100}))

(defn cells->rows
  "Convert intervals to an array of arrays."
  [cells]
  (let [empty-cells (into [] (repeat (dec column-start) nil))
        rows (->> cells
                  (map #(concat empty-cells
                                (vector (:date %)
                                        (:weekday %)
                                        (:holiday %)
                                        (:start-hour-standby %)
                                        (:start-minute-standby %)
                                        (:end-hour-standby %)
                                        (:end-minute-standby %)
                                        (:total-hours-standby %)
                                        (:weekdays-standby %)
                                        (:weekend-standby %)
                                        (:holiday-standby %)
                                        (:start-hour-worked %)
                                        (:start-minute-worked %)
                                        (:end-hour-worked %)
                                        (:end-minute-worked %)
                                        (:premium-50 %)
                                        (:premium-100 %))))
                  (into []))]
    rows))

(defn cells->totals
  "Convert intervals to an array of arrays."
  [cells]
  (let [empty-cells (into [] (repeat (+ 15 (dec column-start)) nil))
        total-premium-50 (->> cells
                              (map :premium-50)
                              (reduce +))
        total-premium-100 (->> cells
                               (map :premium-100)
                               (reduce +))
        totals (-> empty-cells
                   (conj total-premium-50)
                   (conj total-premium-100))]
    totals))

(defn intervals->xlsx
  [intervals]
  "Converts intervals to an Excel workbook.
Loads an Excel template, then fills it with the personal data and calculated
rows, and finally returns a javascript promise."
  (let [name (rf/subscribe [:get-personal-name])
        number (rf/subscribe [:get-personal-number])
        premium (rf/subscribe [:get-personal-premium])
        cells (map process-interval intervals)
        rows (cells->rows cells)
        totals (cells->totals cells)]
    ;; load workbook from xlsx
    (-> (load-xlsx xlsx-path)
        ;; get worksheet from workbook
        (.then (fn [workbook]
                 (let [worksheet (.getWorksheet ^js workbook worksheet-name)
                       name-cell (.getCell ^js worksheet name-ref)
                       number-cell (.getCell ^js worksheet number-ref)
                       premium-pay-cell (.getCell ^js worksheet premium-pay-ref)
                       premium-time-cell (.getCell ^js worksheet premium-time-ref)]
                   ;; Add name en number
                   (set! (.-value name-cell) @name)
                   (set! (.-value number-cell) @number)
                   (case @premium
                     "pay" (do
                             (set! (.-value premium-pay-cell) "Ja")
                             (set! (.-value premium-time-cell) "Nee"))
                     "time" (do
                              (set! (.-value premium-pay-cell) "Nee")
                              (set! (.-value premium-time-cell) "Ja")))
                   ;; Insert the calculated rows
                   (.insertRows ^js worksheet (inc row-start) (clj->js rows) "i+")
                   ;; Delete the empty row above the table
                   (.spliceRows ^js worksheet row-start 1)
                   ;; Insert the calculated totals below the empty totals row
                   (.insertRow ^js worksheet (inc (+ row-start (count rows))) (clj->js totals) "i+")
                   ;; Remove the empty row totals above the totals
                   (.spliceRows ^js worksheet (+ row-start (count rows)) 1)
                   ;; write workbook to buffer
                   (.writeBuffer (.-xlsx ^js workbook)))))
        ;; stringify buffer to base64 string
        (.then (fn [buffer] (.stringify base64 buffer))))))
