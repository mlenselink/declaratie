# Changelog

## [2.0.0] - 2021-12-06
- Upgrade react-native from 0.64.2 to 0.66.3
- Fix race condition when agenda tries to load new items before start and after end at the same time
- Fix bug in editing of Excel sheet

## [1.3.1] - 2021-12-02
- Wait for permissions before boot

## [1.3.0] - 2021-12-01
- Add instructions to grant permissions

## [1.2.0] - 2021-12-01
- Prompt for permissions
- Improve layout

## [1.1.1] - 2021-11-30
- Fix signing config

## [1.1.0] - 2021-11-29
- Fix scrolling of agenda and calendar
- Upgrade react-navigation to v6 (fixes https://github.com/software-mansion/react-native-screens/issues/1197)
- Upgrade all npm packages
- Add configuration for react-native-screens package
- Add debug symbols
- Add signing config
- Upgrade target SDK to 31 to meet Google Play's requirements

## [1.0.0] - 2021-11-04
- Initial Release
