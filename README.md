# Declaratie

**Declaratie** (Dutch for "declaration") is an Android application written in
ClojureScript.

It is a mobile app to fill out time sheets for weekend and night duties,
specifically aimed at doctors working in the [Vincent van Gogh for Mental
Health](https://www.vvgi.nl/) in the Netherlands. As such, it is not suitable
for general use. However, the project could be used as an example of how to use
ClojureScript to build React Native apps.

It uses
[React Native](https://facebook.github.io/react-native/),
[Reagent](https://reagent-project.github.io/),
[re-frame](http://day8.github.io/re-frame/),
[Shadow CLJS](https://github.com/thheller/shadow-cljs),
and a lot of React Native libraries, for example:
[React Navigation](https://reactnavigation.org/),
[React Native Paper](https://callstack.github.io/react-native-paper/),
[React Native Calendars](https://github.com/wix/react-native-calendars),
[React Native Bidirectional Infinite Scroll](https://getstream.github.io/react-native-bidirectional-infinite-scroll/),
[Async Storage](https://react-native-async-storage.github.io/async-storage/),
[react-native-fs](https://github.com/itinance/react-native-fs),
[react-native-background-fetch](https://github.com/transistorsoft/react-native-background-fetch),
[react-native-calendar-events](https://github.com/wmcmahan/react-native-calendar-events),
[react-native-call-log](https://github.com/wscodelabs/react-native-call-log).

## Download

You can download APK's directly from [GitLab releases](https://gitlab.com/fvdbeek/declaratie/-/releases).

## Build
```
# Install dependencies
$ yarn install && cd react-native && yarn install
# Run a release build
$ cd .. && yarn shadow-cljs release app
# Create an Android release build
$ cd react-native/android && ./gradlew assembleRelease
# The APK should appear at android/app/build/outputs/apk/release
```

## Develop
```
# Install dependencies
$ yarn install && cd react-native && yarn install
# Compile and watch
$ cd .. && yarn shadow-cljs watch app
# Start the React Native packager
$ cd react-native && yarn start
# Install and launch the application in an Android emulator or on a device
$ yarn run android
```
Please refer to the [Shadow CLJS user's guide](https://shadow-cljs.github.io/docs/UsersGuide.html) and the [React Native Documentation](https://reactnative.dev/docs/getting-started) for more information.
